<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\QrCodeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SeminarController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\RegisteredUserController;
use App\Http\Controllers\SeminarVideoController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Auth.index');
})->name('login');

Route::get('/registration', function () {
    return view('Auth.register');
})->name('registration');


Route::controller(UserController::class)->group(function () {
	Route::get('registration-verify/{token}','showVerifyEmail')->name('registration_verify.get');
	Route::post('registration','store')->name('registration.store');
    Route::get('registration-success', 'success')->name('registration.success');
});

//Authentications/Login
Route::group(['prefix'=> 'auth'], function(){
	Route::name('auth.')->group(function () {
		// For Login
		Route::controller(AuthController::class)->group(function () {
	 		 Route::post('request', 'authenticate')->name('authenticate');
	 		 Route::get('test', 'test')->name('test');
		});

		// For Forgot Password
		Route::controller(ForgotPasswordController::class)->group(function () {
			Route::get('forget-password', 'index')->name('forget.password.get');
			Route::post('forget-password','store')->name('forget.password.post');
			Route::get('reset-password/{token}', 'showResetPasswordForm')->name('reset.password.get');
			Route::post('reset-password', 'submitResetPasswordForm')->name('reset.password.post');
			Route::get('reset-password-success/', 'resetSuccess')->name('reset.password.success');
			Route::get('reset-password-sent/', 'sentResetPasswordPage')->name('reset.password.sent');
		});
	});});


// Route::get('account/', 'AccountController@index')->name('account.index');

Route::group(['prefix'=> 'account', 'middleware' => ['auth']], function(){

    Route::name('account.')->group(function () {
        // Main Manager Page
        Route::controller(AccountController::class)->group(function () {
            Route::get('','index')->name('index');
        });

        //seminar list
        Route::controller(SeminarController::class)->group(function () {
            Route::get('seminar/{date_from?}/{date_to?}','index')->name('seminar'); //this would be seminar list
            Route::get('seminar-list','list')->name('seminar.list'); //this would be seminar list
            Route::get('seminar-detail/{id?}/{date_from?}/{date_to?}','showSeminarDetail')->name('seminar.detail.get');
            Route::get('seminar-detail-list/{id}','showSeminarDetailList')->name('seminar.detail.list');
            Route::get('seminar-status/{id?}','showSeminarStatus')->name('seminar.status');
            Route::get('seminar-export-excel/{id?}', 'exportSeminarDetail')->name('seminar.export.excel');// Route for export/download tabledata to .csv, .xls or .xlsx

            // Qrcode
            Route::get('seminar-today-listing','seminarToday')->name('seminar.today'); //this is qrcode scanner 
            Route::get('seminar-qrcode/{id?}','qrcodeScanner')->name('seminar.qrcode'); //this is qrcode scanner 
            Route::post('seminar-qrcode-scan','qrcodeScan')->name('seminar.qrcode_scan'); //this qrcode scanner function
            Route::get('seminar-qrcode-status/{status?}/{id?}','qrcodeStatus')->name('seminar.qrcode_status'); //this is qrcode status if time in or time out
            Route::get('seminar-qrcode-error/{id?}','qrcodeStatusError')->name('seminar.qrcode_status_error'); //this is qrcode error
            Route::get('seminar-qrcode-error-code/{id?}','qrcodeStatusErrorSeminar')->name('seminar.qrcode_status_error_seminar'); //this is qrcode error
            Route::get('seminar-qrcode-error-exam/{id?}','qrcodeStatusErrorExam')->name('seminar.qrcode_status_error_exam'); //this is qrcode error
            Route::get('seminar-qrcode-auth/{status?}/{id?}','qrcodeAuth')->name('seminar.qrcode_auth'); //this is qrcode error
            Route::post('seminar-qrcode-authenticate','qrcodeAuthBack')->name('seminar.qrcode_auth_back'); //this is qrcode error


            //search seminar
            Route::post('seminar-filter/{id?}','filterSeminar')->name('seminar.filter');
            Route::get('user-seminar-status/{id?}','getUserSeminarStatus')->name('seminar.user.status');
            Route::post('user-seminar-update-reception','updateUserSeminarReception')->name('seminar.user.seminar.reception.update');
        });

        // User
         Route::controller(RegisteredUserController::class)->group(function () {
            Route::get('email-authentication-status','showUnverifiedUser')->name('user.unverified');
            Route::get('user-unverified','listUnverifiedUser')->name('user.unverified.list');
            Route::get('verify-user/{id?}','verifyUser')->name('user.set.verify');

        });

    });

 	Route::name('seminar_exam.')->group(function () {
        // Seminar Exam
        Route::controller(ExamController::class)->group(function () {
            Route::get('seminar-exam/','index')->name('index');
            Route::get('seminar-exam-list/','list')->name('list');
            Route::get('seminar-exam-show/{seminar_id?}','show')->name('show');
            Route::post('seminar-exam-store/','store')->name('store');


            Route::get('seminar-exam-seminar-detail/{seminar_id?}','seminarDetail')->name('seminar_detail');
            Route::get('seminar-exam-seminar-edit/{seminar_id}','seminar_detail_edit')->name('seminar_edit');
            Route::post('seminar-exam-seminar-edit/{seminar_id}','seminar_detail_edit_save')->name('seminar_edit_save');
            Route::get('seminar-exam-seminar-add/','seminar_add')->name('seminar_add'); 
            Route::post('seminar-exam-seminar-save/','seminarStore')->name('seminar_store'); 

            Route::get('change-exam-answer/','changeExam')->name('change_exam');          
            Route::get('change-exam-answer-list/','changeExamList')->name('exam_list');
            Route::get('change-exam-answer-list-detail/{seminar_registration_id}','changeExamListDetail')->name('exam_list_detail'); 
            Route::post('change-exam-answer-list-detail-save/','changeExamListDetailstore')->name('exam_list_detail_store');   
            // Route::get('change-exam-answer-edit/{id?}','changeExamEdit')->name('exam_edit');

                 
                
        });

    });

    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

});


// video on demand
Route::group(['prefix'=> 'delivery-video', 'as' => 'SeminarVideo.', 'middleware' => ['auth']], function(){
    Route::controller(SeminarVideoController::class)->group(function () {
        Route::get('','index')->name('index');
        Route::get('list','list')->name('list');
        Route::get('details/{seminar_id?}/{id?}','details')->name('details');
        Route::post('save','saveSeminarVideo')->name('save');
        Route::get('upload/{seminar_id?}/{id?}','upload')->name('upload');
        Route::get('delete/{seminar_id?}','deleteFile')->name('delete');
        Route::post('upload','uploadFile')->name('upload.file');
    });
});


