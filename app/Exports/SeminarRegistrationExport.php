<?php

namespace App\Exports;

use App\Models\SeminarRegistration;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SeminarRegistrationExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function collection()
    {   

        $seminar_data = array();

        $seminar_registrations = SeminarRegistration::has('user')->with('user','seminarExams','userExams')->where('m_seminar_id',$this->id)->whereNull('canceled_at')->get();

        if(!$seminar_registrations->isEmpty()){
             foreach($seminar_registrations as $sr){
                $row['registration_at'] = $sr->registration_at;
                $row['l_name'] = $sr->user->l_name;
                $row['f_name'] = $sr->user->f_name;
                $row['l_name_kana'] = $sr->user->l_name_kana;
                $row['f_name_kana'] = $sr->user->f_name_kana;
                $row['certificate_id1'] = $sr->user->certificate_id1;
                $row['registration_id'] = $sr->user->registration_id;
                $row['mail_address'] = $sr->user->mail_address;
                $row['hospital_name'] = $sr->user->hospital_name;
                $row['devision_name'] = $sr->user->devision_name;
                $row['tel'] = $sr->user->tel;
                $row['birthday'] = $sr->user->birthday;
                $row['correct_answers'] = 0;
                $row['no_exam'] = true;
                
                if($sr->seminarExams->isEmpty()){

                    for($i = 1; $i <= 5; $i++){
                        $row['question_'.$i.'_correct'] = '';
                        $row['question_'.$i.'_answer'] = '';
                    }

                }else{

                    foreach($sr->seminarExams as $exam){
                        $row['question_'.$exam->question_no.'_correct'] = '';
                        $row['question_'.$exam->question_no.'_answer'] = '';

                        if(!$sr->userExams->isEmpty()){
                            $row['no_exam'] = false;
                            foreach($sr->userExams as $user_exam){
                                if($user_exam->exam_id == $exam->id){
                                    if($user_exam->correct==1) $row['correct_answers']+=1;
                                    $row['question_'.$exam->question_no.'_correct'] = $user_exam->correct;
                                    $row['question_'.$exam->question_no.'_answer'] = $user_exam->answer;
                                }
                            }
                        }else{
                           $row['question_'.$exam->question_no.'_correct'] = ''; 
                        }

                        if($exam->seminar_use==0) $row['question_'.$exam->question_no.'_correct'] = '';

                        if($row['question_'.$exam->question_no.'_correct']==0) $row['question_'.$exam->question_no.'_correct'] = '誤';
                        elseif($row['question_'.$exam->question_no.'_correct']==1) $row['question_'.$exam->question_no.'_correct'] = '正';
                        else $row['question_'.$exam->question_no.'_correct'] = '';

                        if($row['question_'.$exam->question_no.'_answer']==0) $row['question_'.$exam->question_no.'_answer'] = 'いいえ';
                        elseif($row['question_'.$exam->question_no.'_answer']==1) $row['question_'.$exam->question_no.'_answer'] = 'はい';
                        else $row['question_'.$exam->question_no.'_answer'] = '';
                        
                    }

                }

                if($row['correct_answers']==0 && $row['no_exam']==false) $row['correct_answers'] = "否";
                elseif($row['correct_answers'] > 1 && $row['no_exam']==false) $row['correct_answers'] = '合';
                else $row['correct_answers'] = "否";
                    
                $seminar_data[] = $row;
            }
        }

        return collect($seminar_data);
    }

    public function headings(): array
    {
        return [
            '申込日',
            '氏',
            '名',
            '氏（ヨミガナ）',
            '名（ヨミガナ）',
            '会員番号',
            '参加証番号',
            'Emailアドレス',
            '所属施設',
            '所属部署',
            'TEL',
            '生年月日',
            '問題１',
            '正誤1',
            '問題２',
            '正誤2',
            '問題３',
            '正誤3',
            '問題４',
            '正誤4',
            '問題５',
            '正誤5',
            '合否',
        ];
    }

    public function map($data): array
    {
        return [
            date('Y/m/d', strtotime($data['registration_at'])),
            $data['l_name'],
            $data['f_name'],
            $data['l_name_kana'],
            $data['f_name_kana'],
            $data['certificate_id1'],
            $data['registration_id'],
            $data['mail_address'],
            $data['hospital_name'],
            $data['devision_name'],
            $data['tel'],
            $data['birthday'],
            $data['question_1_answer'],
            $data['question_1_correct'],
            $data['question_2_answer'],
            $data['question_2_correct'],
            $data['question_3_answer'],
            $data['question_3_correct'],
            $data['question_4_answer'],
            $data['question_4_correct'],
            $data['question_5_answer'],
            $data['question_5_correct'],
            $data['correct_answers'],
        ];
    }
}
