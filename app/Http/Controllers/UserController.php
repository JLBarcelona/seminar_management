<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use App\Mail\VerifyRegistration;
use Validator;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return view('Auth.register');
    }

    public function store(Request $request)
    {

        $message = ['tel.regex' => '数字とハイフンで入力してください'];

        $validator = Validator::make($request->all(), [
            'l_name' => 'required',
            'f_name' => 'required',
            'l_name_kana' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u',
            'f_name_kana' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u',
            'certificate_id1' => 'required|regex:/^[ァ-ヶ゛゜ァ-ォャ-ョー()（）]+$/u|max:1',
            'certificate_id2' => 'required|max:4',
            'mail_address' => 'required|email|unique:users,mail_address',
            'hospital_name' => 'required',
            'devision_name' => 'required',
            'tel' => 'required|regex:/^\d{2,4}-\d{2,6}-\d{2,6}$/|max:16',
            'birthday' => 'required',
            'password' => 'required|min:8|max:8|regex:/^[a-zA-Z0-9]+$/u',
        ],$message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            User::create($request->all());
            $token = Str::random(64);

            $update = User::where('mail_address', $request->mail_address)->update(['email_verification_token'=>$token]);

            if($update){
                $url = route('registration_verify.get', ['token' => $token]);
                $data = ['url' => $url,
                        'title' => '+Seminar メールアドレス確認',
                        'support_name' => '',
                        'support_mail_address' =>'',
                        'subject' => '下記URLよりメールアドレスの確認をお願い致します。'
                    ];

                Mail::to($request->mail_address)->send(new VerifyRegistration($data));

                return response()->json(['status' => true, 'redirect' => route('registration.success')]);
            }

        }
    }

    public function showVerifyEmail($token){
        $user_data = User::select('id','mail_address','email_verification_token')->where('email_verification_token', $token)->first();

        if($user_data){
            $update = User::where('mail_address', $user_data->mail_address)->update(['email_verification_token'=>NULL, 'email_check_at'=>date('Y-m-d H:i:s')]);
            return view('Auth.email_verify');
        }
    }

    public function success(){
        return view('Auth.success');
    }

}
