<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\RegisteredUser;

use Validator;

use App\Models\User;

class RegisteredUserController extends Controller
{
    public function index()
    {
        return view('Auth.register');
    }

    public function showUnverifiedUser(){
       return view('Account.email_authentication');
    }

    public function listUnverifiedUser(){
        $user = RegisteredUser::whereNotNull('email_verification_token')->get();
        return response()->json(['status' => true, 'data' => $user]);
    }


    public function verifyUser($id){

        $user = RegisteredUser::findOrfail($id);
        $user->email_check_at = now();
        $user->email_verification_token = null;

        if ($user->save()) {
            return redirect(route('account.user.unverified'));
        }
    }

}
