<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\Models\RegisteredUser;
use App\Models\Seminar;
use App\Models\SeminarRegistration;
use App\Models\UserSeminarStatus;

use App\Exports\SeminarRegistrationExport;
use Maatwebsite\Excel\Facades\Excel;

use \Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

use DataTables;

class SeminarController extends Controller
{
    public function index($date_from='',$date_to=''){
        return view('Account.Seminar.index',compact('date_from','date_to'));
    }

    public function seminarToday(){
        $seminars = Seminar::whereDate('seminar_date', date('Y-m-d'))->get();
        return view('Account.Seminar.seminar_today', compact('seminars'));
    }

    public function qrcodeScanner($id){
        $seminars = Seminar::with(['seminarGroup'])->findOrFail($id);
        return view('Account.Seminar.qrcode_scan', compact('seminars'));
    }

    public function qrcodeAuth($status = 'main-menu'){
        $redirect = ($status == 'status-change')? route('account.seminar.status') : route('account.index');
        return view('Account.Seminar.qrcode_auth', compact('redirect'));
    }

    public function qrcodeAuthBack(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'password'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors() ]);
        }else{
            if (!Hash::check($request->get('password'), $user->password)) {
                $validator->errors()->add('password','パスワードが違います');
                return response()->json(['status' => false, 'error' => $validator->errors()]);
            }else{
                return response()->json(['status' => true]);
            }
        }
    }

    public function qrcodeScan(Request $request){
        $seminar_id = $request->get('seminar_id');

        if (strtolower($request->get('qr_code')) == 'error') {
             return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status_error', $seminar_id)]);
        }else{
            $has_seminar_registration = SeminarRegistration::where('qr_code', $request->get('qr_code'));
                /*check if there's a qrcode registered on scanner*/
            if ($has_seminar_registration->count() > 0) {
                $seminar_registration = $has_seminar_registration->first();
                $has_status = UserSeminarStatus::where('seminar_registration_id', $seminar_registration->id);
              

                if ($seminar_registration->m_seminar_id == $seminar_id) {
                      /*check if there's already time in*/
                    if ($has_status->count() > 0) {
                        $seminar_status = $has_status->first();

                        if (!empty($seminar_status->exam_answered_at)) {
                            $time_in = Carbon::parse($seminar_status->reception_in_at);
                            $current = Carbon::now();
                        
                              /*Check if scanned less than 30 seconds ago*/
                            if ($current->diffInSeconds($time_in) < 30) { 
                                    return response()->json(['status' => false, 'message' => 'You just scanned '.$current->diffInSeconds($time_in).' seconds ago!']);
                            }else{
                                    /*check if threre's now time out */
                                if (empty($seminar_status->reception_out_at)) {
                                    /*Register the time out*/
                                    $seminar_status->reception_out_at = now();
                                    if ($seminar_status->save()) {
                                        return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status', ['Out', $seminar_id])]);
                                    }
                                }else{
                                    /*check if threre's now time out */
                                     return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status_error', $seminar_id)]);
                                }
                            }
                        }else{
                            return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status_error_exam', $seminar_id)]);
                        }

                    }else{
                        /*Register the time in*/
                        $time_in = UserSeminarStatus::insert(['seminar_registration_id' => $seminar_registration->id, 'reception_in_at' => now()]);
                        if ($time_in) {
                            return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status', ['timeIn', $seminar_id])]);
                        }
                    }
                  
                }else{
                     // return 1;
                    // return $seminar_id.' '.$seminar_registration->m_seminar_id;
                    return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status_error_seminar', $seminar_id)]);
                }
            }else{
                /*return error if qrcode is not registered*/
                 return response()->json(['status' => true, 'message' => '', 'redirect' => route('account.seminar.qrcode_status_error', $seminar_id)]);
            }
        }
    }

    public function qrcodeStatusError($id){
        return view('Account.Seminar.qrcode_error', compact('id'));
    }

    public function qrcodeStatusErrorSeminar($id){
        return view('Account.Seminar.qrcode_error_seminar', compact('id'));
    }

    public function qrcodeStatusErrorExam($id){
        return view('Account.Seminar.qrcode_error_exam', compact('id'));
    }

    public function qrcodeStatus($status = 'timeIn', $id){
        $message = ($status === 'timeIn')? 'ご来場ありがとうございます' : '退席確認が完了しました';
        $status_message = ($status === 'timeIn') ? '参加受付が完了しました' : '退席確認が完了しました';
        $page_image = ($status === 'timeIn')? '2' : '3';
        return view('Account.Seminar.qrcode_success', compact('message', 'status_message', 'page_image', 'id'));
    }

    public function list(Request $request){
    	$from = $request->get('date_from');
    	$to = $request->get('date_to');
        $date_now = date('Y-m-d');
		$query = [];

        if($from=='empty') $from='';
        if($to=='empty') $to='';

        $query = Seminar::select();

        if(!empty($from) && empty($to)){
            $query->whereBetween('seminar_date', [$from, $date_now]);
        }elseif(empty($from) && !empty($to)){
            $query->whereDate('seminar_date','<=',$to);
        }elseif(!empty($from) && !empty($to)){
            $query->whereBetween('seminar_date', [$from, $to]);
        }

        $query->withCount(['seminar_registration' => function($q) {
               $q->has('user')->whereNull('canceled_at');
            }]);

		return Datatables::of($query->get())->make(true);
    }

    public function showSeminarStatus(){
        return view('Account.Seminar.seminar_status');
    }

    public function showSeminarDetail($id,$date_from='',$date_to=''){

        $seminar = Seminar::find($id);
        return view('Account.Seminar.detail',compact('seminar','date_from', 'date_to'));
 
    }

    public function showSeminarDetailList(Request $request, $id){
        $seminar_data = array();

        $seminar_registrations = SeminarRegistration::has('user')->with('user','seminarExams','userExams')->where('m_seminar_id',$id)->whereNull('canceled_at')->get();

        if(!$seminar_registrations->isEmpty()){
            foreach($seminar_registrations as $sr){
                $row['registration_at'] = $sr->registration_at;
                $row['l_name'] = $sr->user->l_name;
                $row['f_name'] = $sr->user->f_name;
                $row['l_name_kana'] = $sr->user->l_name_kana;
                $row['f_name_kana'] = $sr->user->f_name_kana;
                $row['certificate_id1'] = $sr->user->certificate_id1;
                $row['registration_id'] = $sr->user->registration_id;
                $row['mail_address'] = $sr->user->mail_address;
                $row['hospital_name'] = $sr->user->hospital_name;
                $row['devision_name'] = $sr->user->devision_name;
                $row['tel'] = $sr->user->tel;
                $row['birthday'] = $sr->user->birthday;
                $row['correct_answers'] = 0;
                $row['no_exam'] = true;

                if($sr->seminarExams->isEmpty()){

                    for($i = 1; $i <= 5; $i++){
                        $row['question_'.$i.'_correct'] = '';
                        $row['question_'.$i.'_answer'] = '';
                    }

                }else{

                    foreach($sr->seminarExams as $exam){
                        $row['question_'.$exam->question_no.'_correct'] = '';
                        $row['question_'.$exam->question_no.'_answer'] = '';

                        if(!$sr->userExams->isEmpty()){
                            $row['no_exam'] = false;
                            foreach($sr->userExams as $user_exam){
                                if($user_exam->exam_id == $exam->id){
                                    if($user_exam->correct==1) $row['correct_answers']+=1;
                                    $row['question_'.$exam->question_no.'_correct'] = $user_exam->correct;
                                    $row['question_'.$exam->question_no.'_answer'] = $user_exam->answer;
                                }
                            }
                        }else{
                           $row['question_'.$exam->question_no.'_correct'] = ''; 
                        }

                        if($exam->seminar_use==0) $row['question_'.$exam->question_no.'_correct'] = '';

                        if($row['question_'.$exam->question_no.'_correct']==0) $row['question_'.$exam->question_no.'_correct'] = '誤';
                        elseif($row['question_'.$exam->question_no.'_correct']==1) $row['question_'.$exam->question_no.'_correct'] = '正';
                        else $row['question_'.$exam->question_no.'_correct'] = '';

                        if($row['question_'.$exam->question_no.'_answer']==0) $row['question_'.$exam->question_no.'_answer'] = 'いいえ';
                        elseif($row['question_'.$exam->question_no.'_answer']==1) $row['question_'.$exam->question_no.'_answer'] = 'はい';
                        else $row['question_'.$exam->question_no.'_answer'] = '';
                        
                    }

                }
                
                if($row['correct_answers']==0 && $row['no_exam']==false) $row['correct_answers'] = "否";
                elseif($row['correct_answers'] > 1 && $row['no_exam']==false) $row['correct_answers'] = '合';
                else $row['correct_answers'] = "否";
                
                $seminar_data[] = $row;
            }
        }

		return Datatables::of($seminar_data)->make(true);
    }

    public function filterSeminar(Request $request){

        $certificate_id1 = $request->get('certificate_id1');
        $certificate_id2 = $request->get('certificate_id2');

        $qr_count = 0;
        if($request->filled('qr_1')) $qr_count = $qr_count + 1;
        if($request->filled('qr_2')) $qr_count = $qr_count + 1;
        if($request->filled('qr_3')) $qr_count = $qr_count + 1;
        if($request->filled('qr_4')) $qr_count = $qr_count + 1;

        if(empty($request->get('qr_1')) && empty($request->get('qr_2')) && empty($request->get('qr_3')) && empty($request->get('qr_4'))){
            $qrcode = null;
        }else{
            if($qr_count < 4){
                $qrcode = null;
                return response()->json(['status' => false, 'qrcode_error' => true, 'message' => 'QRコードの検索は４項目全て入力してください']);
            }else{
                $qrcode = $request->get('qr_1').','.$request->get('qr_2').','.$request->get('qr_3').','.$request->get('qr_4');
            }
        }
        $date_now = date('Y-m-d');
        $date_yesterday = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date_now) ) ));
        $date_tomorrow = date('Y-m-d',(strtotime ( '+1 day' , strtotime ( $date_now) ) ));
        $query = SeminarRegistration::whereNull('canceled_at')->whereHas('seminar', function($q) use($date_yesterday,$date_tomorrow) {
                 $q->whereBetween('seminar_date', [$date_yesterday, $date_tomorrow]);})
                 ->whereHas('user', function($q) use($certificate_id1,$certificate_id2,$request,$qrcode) {
                    $full_name = str_replace(['　', ' '], '',$request->get('user_fullname'));
                    if($request->filled('certificate_id1')) $q->where('certificate_id1',$certificate_id1);
                    if($request->filled('certificate_id2')) $q->where('certificate_id2',$certificate_id2);
                    if($request->filled('user_fullname')) $q->where(DB::raw("concat(l_name, f_name)"),'like','%' .$full_name. '%');
                    if(!empty($qrcode)) $q->where('qr_code',$qrcode);
                 })->has('user')->with(['user','seminar']);

        if($query->count() > 0){
            return response()->json(['status' => true, 'data' => $query->get()]);
        }else{
            return response()->json(['status' => false, 'no_data' => true, 'message' => "該当のデータはありません"]);
        }

    }

    public function getUserSeminarStatus($id){
        $user_seminar_status = UserSeminarStatus::where('seminar_registration_id',$id)->first();
        return response()->json(['status' => true, 'data' => $user_seminar_status]);
    }

    public function updateUserSeminarReception(Request $request){

        $id = $request->get('user_seminar_status_id');
        $type = $request->get('type');
        $seminar_registration_id = $request->get('seminar_registration_id');

        $user_seminar_status = UserSeminarStatus::where('seminar_registration_id',$seminar_registration_id)->first();

        $invalid_button_push = false;

        if($user_seminar_status){
            //check invalid button submit
            if($type == 'reception_in_at'){
                if(!empty($user_seminar_status->reception_in_at)){
                    $invalid_button_push = true;
                }else{
                    $invalid_button_push = false;
                }
            }elseif($type == 'reception_out_at'){
                if(empty($user_seminar_status->reception_in_at) || !empty($user_seminar_status->reception_out_at)){
                    $invalid_button_push = true;
                }else{
                    $invalid_button_push = false;
                }
            }

            if($invalid_button_push == false){
                $query = UserSeminarStatus::where('id',$id)->update([$type => now()]);
            }else{
                return response()->json(['status' => false, 'type' => $type, 'message' => 'invalid push button']);
            }

            if($query){
                return response()->json(['status' => true, 'type' => $type, 'user_seminar_status_id' => $id]);
            }

        }else{

            $data = new UserSeminarStatus;
            $data->seminar_registration_id = $seminar_registration_id;
            $data->reception_in_at = now();
            $query = $data->save();
            $id = $data->id;
            if($query){
                return response()->json(['status' => true, 'type' => $type, 'user_seminar_status_id' => $id]);
            }

        }
    }

    public function exportSeminarDetail($id)
    {
        $seminar = Seminar::find($id);
        $filename = date('Ymd', strtotime($seminar->seminar_date)).'_'.$seminar->category_no.'_'.$seminar->category_name.'.csv';
        return Excel::download(new SeminarRegistrationExport($id), $filename);
    }

}
