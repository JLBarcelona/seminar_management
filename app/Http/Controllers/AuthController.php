<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use App\Models\User;
use Validator;


class AuthController extends Controller
{


	public function index(){
		if (Auth::check()) {
			return redirect(route('account.index'));
		}else{
			return view('Auth.index');
		}
	}

	public function authenticate(Request $request){
		 $validator = Validator::make($request->all(), [
            'login_id' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	$credentials = $request->validate([
	            'login_id' => ['required'],
	            'password' => ['required'],
	        ]);

	        if (Auth::attempt($credentials)) {
	            $request->session()->regenerate();
	            $user = Auth::user();
	            if (!empty($user->company->company->credit_card_number) && !empty($user->company->company->card_name)) {
            		Session::flush();
					Auth::logout();
		            return response()->json(['status' => true, 'redirect' => route('login')]);
            	}else{
		         	// if (save_auth_logs($user->id)) {
		          //   }
		            return response()->json(['status' => true, 'redirect' => route('account.index')]);

            	}
	        }else{
	        	$validator->errors()->add('password','パスワードが違います');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	        }
        }
	}

	public function logout()
    {
		$user = Auth::user();
		Session::flush();
    	Auth::logout();
		return redirect('/');
		// if(save_auth_logs($user->id,'logout')) {
			
		// }
    }
}