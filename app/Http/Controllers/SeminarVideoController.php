<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\SeminarVideo;
use App\Models\Seminar;
use Validator;
use File;

class SeminarVideoController extends Controller
{
    public function index(){
        return view('SeminarVideo.index');
    }

    public function saveSeminarVideo(Request $request){

        $seminar_id = $request->get('seminar_id');
        $id = $request->get('id');
        $video_open_at = $request->get('video_open_at');
        $video_close_at = $request->get('video_close_at');

        $validator = Validator::make($request->all(), [
            'video_open_at' => 'required',
            'video_close_at' => 'required',
            'upload_file' => 'required'
        ],[
            'upload_file.required' => '動画がアップロードされていません'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);   
        }else{

            if (!empty($seminar_id)) {
                $video =  SeminarVideo::where('m_seminar_id', $seminar_id)->first();
                $video->video_open_at = $video_open_at;
                $video->video_close_at = $video_close_at;
                if ($video->save()) {
                    return response()->json(['status' => true, 'redirect' => route('SeminarVideo.index')]);
                }
            }else{
                $video = new SeminarVideo;
            }
            $video->video_open_at = $video_open_at;
            $video->video_close_at = $video_close_at;
            if ($video->save()) {
                return response()->json(['status' => true, 'redirect' => route('SeminarVideo.index')]);
            }

        }
    }

    public function details($seminar_id, $id = '', Request $request){
        $details = [];

        if (!empty($id)) {
        $details = Seminar::with(['video' => function($q) use($id){
                $q->where('id', $id);
            }])->where('id', $seminar_id)->orderBy('start_at', 'desc')->firstOrFail();
        }else{
         $details = Seminar::with(['video'])->where('id', $seminar_id)->orderBy('start_at', 'desc')->firstOrFail();
        }

        return view('SeminarVideo.details', compact('details' , 'seminar_id', 'id', 'request'));
    }


    public function upload($seminar_id, $id = '', Request $request){
        $details = (!empty($id))? SeminarVideo::find($id): [];
        return view('SeminarVideo.upload', compact('id' , 'seminar_id' , 'details', 'request'));
    }

    public function uploadFile(Request $request){
        $seminar_id = $request->get('seminar_id');
        $file_name = $request->get('file_name');
        $flag = $request->get('flag');
        $video_id = $request->get('video_id');
        $video_times = $request->get('video_times');

       $validator = Validator::make($request->all(), [
            'upload_file' => 'required',
            'video_times' => 'required'
       ]); 

       if ($validator->fails()) {
           return response()->json(['status' => false, 'error' => $validator->errors()]);
       }else{

        $has_video = SeminarVideo::where('m_seminar_id', $seminar_id);
        if ($has_video->count() > 0) {
            $video = $has_video->first();
        }else{
            $video = new SeminarVideo;
        }
            

        $new_file_name = str_replace([' ', '\'', '(', ')', '-', '"'], '_', $file_name);

        $file = request()->file('upload_file');
        $file->storePubliclyAs($seminar_id, $new_file_name, 'video');
        $video->m3u8file_path = $new_file_name;
        $video->m_seminar_id = $seminar_id;
        $video->keep_original_flag = $flag;
        $video->video_times = $video_times;
        if ($video->save()) {

             $path = public_path('/video/'.$video->m_seminar_id);
             $source_path = str_replace('\\', '/', $path);

            exec('ffmpeg -i '.$path.'/'.$video->m3u8file_path.' -c:v copy -c:a copy -f hls -hls_time 9 -hls_playlist_type vod -hls_segment_filename "'.$path.'/video%3d.ts" '.$path.'/video.m3u8', $output, $intercount);

            if ($intercount == 0) {
                if ($flag !== 1) {
                    if (File::exists($source_path.'/'.$new_file_name)) {
                        File::deleteDirectory($source_path.'/'.$new_file_name);
                    }    
                }

                return response()->json(['status' => true, 'redirect' => route('SeminarVideo.details', [$seminar_id, $video->id]) ]);
            }else{
                $validator->errors()->add('upload_file','Failed to upload file. please try again.');
                return response()->json(['status' => false, 'error' => $validator->errors()]);
            }
             

        }
       }
    }

    public function deleteFile($id){
        $has_video = SeminarVideo::where('id', $id);
        if ($has_video->count() > 0) {
            $video = $has_video->first();
            
            $source_path = public_path('/video/'.$video->m_seminar_id.'/'.$video->m3u8file_path);
            // $source_path = str_replace('/', '\\', $path);

            if (File::exists($source_path)) {
                File::deleteDirectory($source_path);
            }    

            $video->m3u8file_path = null;
            $video->video_open_at = null;
            $video->video_close_at = null;
            $video->keep_original_flag = null;
            $video->video_times = null;
            if ($video->save()) {
                 return redirect(route('SeminarVideo.details', [$video->m_seminar_id, $id]));
             }
        }

    }


    public function list(){
        $today = now();
        $seminar = Seminar::with(['video' => function($q) use($today){
            $q->where('video_close_at', '>=', date('Y-m-d', strtotime($today)))
            ->orWhere(function($query){
                $query->whereNull('video_close_at');
            });
        }])->orderBy('start_at', 'desc')->get();

        return response()->json(['status' => true, 'data' => $seminar]);
    }
}
