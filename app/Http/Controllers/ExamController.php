<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Seminar;
use App\Models\Exam;
use App\Models\UserExam;
use App\Models\SeminarGroup;
use App\Models\SeminarRegistration;
use DataTables;
class ExamController extends Controller
{

    public function index(){

        $user = Auth::user();
        return view('Exam.index');
    }

    public function list(Request $request){
        $today = date('Y-m-d');
        $query = [];

        if($request->checkbox_con=='true'){
             $query = Seminar::orderBy('m_seminar_group_id','desc')->orderBy('category_no','asc');
        }else{
             $query = Seminar::where('seminar_date', '>=', $today)->orderBy('m_seminar_group_id')->orderBy('category_no');
        }

        return Datatables::of($query->get())->make(true);
    }

    public function show($seminar_id){

        $seminar = Seminar::with(["exams" => function($q){ 
                    $q->orderBy('question_no');
                }])->find($seminar_id);

        if(!$seminar){
            return redirect()->route('seminar_exam.index');
        }
            
        if($seminar->exams->isEmpty()){
            //insert to exams

            $exam_data = array();
            for($i = 1; $i <= 5; $i++){

                $field['m_seminar_id'] = $seminar->id;
                $field['question_no'] = $i;
                $field['seminar_use'] = 0;
                $field['answer'] = 0;
                $field['created_at'] = now();
                $exam_data[] = $field;

            }

            Exam::insert($exam_data);
            $seminar = Seminar::with(["exams" => function($q){ 
                    $q->orderBy('question_no');
                }])->find($seminar_id);

        }

        return view('Exam.show',compact('seminar'));

    }

    public function store(Request $request){

        $user = Auth::user();

        if(!empty($request->seminar_use)){

            if(count($request->seminar_use) > 3 || count($request->seminar_use) < 3 ){
                return response()->json(['status' => false, 'message' => '講座で使用する問題は３つ選択してください']);
            }
        } 
        
        $exam_questions_count = 0;

        foreach($request->question_no as $key => $q){
            if(!empty($request->exam_question[$key]) && $request->exam_question[$key]!=''){
                $exam_questions_count +=1;
            }
        }

        if($exam_questions_count < 3){
            return response()->json(['status' => false, 'message' => '少なくとも 3 つの質問に入力して保存してください。']);
        }
        
        foreach($request->question_no as $key => $q){
            $seminar_use = 0;

            if(!empty($request->seminar_use) && !empty($request->seminar_use[$key])) $seminar_use = 1;

            Exam::where('id',$key)->update([
                'answer' => $request->answer[$key],
                'seminar_use' => $seminar_use,
                'exam_question' => $request->exam_question[$key],
                'updated_at' => now()
            ]);
        }

        return response()->json([
            'status' => true, 
            'message' => 'success', 
            'redirect' => route('seminar_exam.index')
        ]);


    }

    /**--- START Seminar Functions ---**/

    public function seminarDetail($seminar_id){
        $seminar = Seminar::with('seminarGroup')->where('id', $seminar_id)->firstorfail();

        return view('Exam.seminar_detail', compact('seminar'));
    }
    
    public function seminar_detail_edit($seminar_id){
        $seminar = Seminar::with('seminarGroup')->find($seminar_id);
        return view('Exam.seminar_detail_edit', compact('seminar'));
    }


    public function seminar_detail_edit_save(Request $request, $seminar_id){
        $validator = Validator::make($request->all(),[
            'category_no' => 'required',
            'category_name' => 'required',
            'seminar_title' => 'required',
            'teacher_name' => 'required',
            'teacher_affiliation1' => 'required',
            'place' => 'required',
            'capacity' => 'required',
            'registration_from' => 'required',
            'registration_to' => 'required',
            'seminar_date' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
            'reception_open_at' => 'required',
            'reception_close_at' => 'required',
        ]);

        if ($validator->fails()) {
            return  response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $seminar = Seminar::where('id', $seminar_id);

            $seminar->update(['category_no' => $request->category_no,
                     'category_name' => $request->category_name,
                     'seminar_title' => $request->seminar_title,
                     'teacher_name' => $request->teacher_name,
                     'teacher_affiliation1' => $request->teacher_affiliation1,
                     'place' => $request->place,
                     'capacity' => $request->capacity,
                     'seminar_date' => $request->seminar_date,
                     'start_at' => date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->start_at)),
                     'end_at' => date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->end_at)),
                     'reception_open_at' => date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->reception_open_at)),
                     'reception_close_at' => date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->reception_close_at)),]);

            $seminar->first()->seminarGroup()->update(['registration_from' =>$request->get('registration_from'), 'registration_to' => $request->get('registration_to')]);
            if ($seminar) {
                return response()->json(['status' => true, 'redirect' => route('seminar_exam.seminar_detail', $seminar_id) ]);
            }
        }

    }

    public function seminar_add(){
        return view('Exam.seminar_add');
    }

    public function seminarStore(Request $request){

        $user = Auth::user();

        $rules = [ 
           'category_no' => 'required',
            'category_name' => 'required',
            'seminar_title' => 'required',
            'teacher_name' => 'required',
            'teacher_affiliation1' => 'required',
            'place' => 'required',
            'capacity' => 'required',
            'registration_from' => 'required',
            'registration_to' => 'required',
            'seminar_date' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
            'reception_open_at' => 'required',
            'reception_close_at' => 'required',
        ];

        $message = [];

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }

        $request->start_at = date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->start_at));
        $request->end_at = date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->end_at));
        $request->reception_open_at = date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->reception_open_at));
        $request->reception_close_at =  date('Y-m-d H:i:s', strtotime($request->seminar_date.' '.$request->reception_close_at));

        DB::beginTransaction();

        //create seminar group
        $seminar_group = SeminarGroup::create([
            'group_name' => $request->category_name,
            'registration_from' => $request->registration_from,
            'registration_to' => $request->registration_to,
        ]);

        //create seminars
        try {
           $seminar_group->seminars()->create($request->all());
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            DB::rollBack();
            return response()->json(['status' => false, 'type' => 'dberror','error' => $errorInfo]);
        }

        //create seminar exams
        $exam_data = array();
        try {
           for($i = 1; $i <= 5; $i++){

                $field['m_seminar_id'] = $seminar_group->seminars[0]->id;
                $field['question_no'] = $i;
                $field['seminar_use'] = 0;
                $field['answer'] = 0;
                $field['created_at'] = now();
                $exam_data[] = $field;

            }
            Exam::insert($exam_data);
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            DB::rollBack();
            return response()->json(['status' => false, 'type' => 'dberror','error' => $errorInfo]);
        }
        
        DB::commit();

        return response()->json(['status' => true, 'redirect' => route('seminar_exam.index')]);

    }

    /**--- END Seminar Functions ---**/

    /**--- Start Change Exam Answer---**/

    public function changeExam(){
        return view('Exam.change_exam');
    }

    public function changeExamList(Request $request){
        $seminar_data = array();

        $seminar_registrations = SeminarRegistration::whereHas('user', function($q) use($request) {
                    if($request->filled('fullname') && $request->fullname!=''){
                        $q->where(DB::raw("concat(l_name,'', f_name)"),'like','%'.str_replace(['　', ' '], '',$request->fullname).'%');
                    }})
                    ->whereHas('seminar', function($q) use($request) {
                        if($request->filled('date_from') && $request->filled('date_to')){
                            $q->whereBetween('seminar_date',[$request->date_from , $request->date_to]);
                        }})
                    ->has('userExams')
                    ->with(['user','seminarExams','userExams','seminar'])
                    ->whereNull('canceled_at')->get();

        if(!$seminar_registrations->isEmpty()){
            foreach($seminar_registrations as $sr){
                $row['user_name'] = $sr->user->l_name . " " . $sr->user->f_name;
                $row['seminar_date'] = $sr->seminar->seminar_date;
                $row['category_no'] = $sr->seminar->category_no;
                $row['category_name'] = $sr->seminar->category_name;
                $row['seminar_registration_id'] = $sr->id;
                
                foreach($sr->userExams as $key => $user_exam){
                    if($user_exam->answer==1) $row['answer_'.$key+=1] = 'はい';
                    else $row['answer_'.$key+=1] = 'いいえ';
                }
            
                $seminar_data[] = $row;
            }
        }

        // $seminar_data;
        return Datatables::of($seminar_data)->make(true);
    }

    public function changeExamListDetail($seminar_registration_id){
        
        $seminar_registration = SeminarRegistration::has('user')->has('userExams')->with(['user','userExams','seminar'])
        ->whereNull('canceled_at')->where('id',$seminar_registration_id)->first();
        return view('Exam.edit_exam',compact('seminar_registration'));
    }

    public function changeExamListDetailstore(Request $request){

        $user = Auth::user();

        //verify seminar registration
        $seminar_registration = SeminarRegistration::has('user')->has('userExams')->with(['user','seminarExams','userExams','seminar', ])
        ->whereNull('canceled_at')->where('id',$request->seminar_registration_id)->first();

        if($seminar_registration){

            foreach($seminar_registration->seminarExams as $exam){
            
                foreach($request->user_exam_id as $key => $q){
          
                    if($request->exam_id[$key] == $exam->id){
                        if($exam->answer == $request->answer[$key]) $is_correct = 1;
                        else $is_correct = 0;
                        UserExam::where('id',$key)->update([
                            'answer' => $request->answer[$key],
                            'correct' => $is_correct,
                            'updated_at' => now()
                        ]);
                    } 
                }
                
            }
            
            return response()->json([
                'status' => true, 
                'message' => 'success', 
                'redirect' => route('seminar_exam.change_exam')
            ]);
        }

        abort(404);

    }

    /**--- End Change Exam Answer---**/

      


}
