<?php

namespace App\Exports;

use App\SeminarRegistration;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class InquiryExport implements FromQuery, WithHeadings, WithColumnWidths
{
    use Exportable;

    public function __construct()
    {
        // $this->date_from = $date_from;
        // $this->date_to = $date_to;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 30,
            'C' => 30,
            'D' => 30,            
            'E' => 30,            
            'F' => 30,            
            'G' => 30,            
            'H' => 30,            
            'I' => 30,            
            'J' => 30,            
            'K' => 30,            
            'L' => 30,            
        ];
    }

    public function query(){
          // return Inquiry::select(DB::raw("concat(first_name,' ', last_name) as Name"), 'email', DB::raw("concat('(',country_code,') ',contact_number) as Contact"),'message')->whereNull('deleted_at')->where(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), ">=", $this->date_from)->where(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), "<=", $this->date_to)->orderBy('status', 'DESC')->orderBy('created_at','DESC');
    }

    public function headings(): array
    {
        return ["Name", "Email Address", "Contact Number", "Message"];
    }
}