<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExam extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'user_exams';

    protected $fillable = [
        'user_id',
        'seminar_registration_id',
        'exam_id',
        'answer',
        'correct',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];


    public function exam_info(){
        return $this->belongsTo('App\Models\Exam', 'exam_id', 'id');
    }

}
