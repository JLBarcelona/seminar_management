<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarGroup extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'm_seminar_groups';

    protected $fillable = [
        'group_name',
        'registration_from',
        'registration_to',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function seminars(){
        return $this->hasMany('App\Models\Seminar', 'm_seminar_group_id', 'id');
    }
}
