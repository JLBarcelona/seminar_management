<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisteredUser extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'l_name',
        'f_name',
        'l_name_kana',
        'f_name_kana',
        'certificate_id1',
        'certificate_id2',
        'registration_id',
        'mail_address',
        'hospital_name',
        'devision_name',
        'tel',
        'birthday',
        'password',
        'email_check_at',
        'email_verification_token',
        'password_reset_parameter',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */



}
