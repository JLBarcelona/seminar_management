<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarCertificate extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'seminar_certificates';

    protected $fillable = [
        'm_seminar_group_id',
        'm_seminar_id',
        'user_id',
        'total_exam_count',
        'correct_answer_count',
        'exam_at',
        'passed_exam',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
