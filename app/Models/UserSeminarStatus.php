<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSeminarStatus extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    
    protected $table = 'user_seminar_status';

    protected $fillable = [
        'seminar_registration_id',
        'reception_in_at',
        'reception_in_err',
        'exam_answered_at',
        'reception_out_at',
        'reception_out_err',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
