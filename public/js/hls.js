class HlsClient {

    #video;
    #arrSkipInfo;
    #eventHMovieEndFunc;

    constructor(view, width, height, skipInfo, eventHMovieEndFunc) {

        this.#arrSkipInfo = skipInfo;
        this.#eventHMovieEndFunc = eventHMovieEndFunc;

        this.#video = videojs(view, {
            width: width,
            height: height,
            loop: false,
            controls: false,
            preload: 'auto'
        });

        this.#video.on(['loadstart', 'loadedmetadata', 'loadeddata',
                'play', 'playing', 'pause', 'suspend', 'seeking', 'seeked',
                'waiting', 'canplay', 'canplaythrough', 'ratechange',
                'ended', 'emptied', 'error', 'abort'], (e) => {
            console.log(`EVENT: ${e.type}`);
            if (e.type == 'ended') {
                this.#eventHMovieEndFunc();
            }
        });
    }

    setSrc(src) {
        this.#video.src({
            type: 'application/x-mpegURL',
            src: src
        });
    }

    play() {
        this.#video.play();
    }

    skip() {
        for (let idx = 0; idx < this.#arrSkipInfo.length; idx++) {
            if (this.#video.currentTime() < this.#arrSkipInfo[idx]) {
                this.#video.currentTime(this.#arrSkipInfo[idx]);
                break;
            }
        }
    }
}