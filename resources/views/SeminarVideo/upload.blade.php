@extends('Layout.account.app')
@section('title', '動画アップロード')

@section('css')
@endsection

@section('content')
<form class="needs-validation" id="form_upload" action="{{ route('SeminarVideo.upload.file') }}" enctype="multipart/form-data" novalidate="">
    <div class="row justify-content-center mt-5">
      <div class="col-md-10 col-12">
          <div class="row justify-content-center">
          <!-- form label start -->
          <div class="col-sm-12">
            <label class="page-title">動画アップロード</label>
          </div>

          <div class="col-sm-12">
            <div class="row">
              <div class="input-group col-md-6 mb-3">
                <input type="file" name="upload_file" id="upload_file" class="d-none" required="" accept="video/*">
                <input type="hidden" name="seminar_id" id="seminar_id" class="d-none" value="{{ $seminar_id }}" readonly="">
                <input type="hidden" name="video_id" id="video_id" class="d-none" value="{{ $id ?? '' }}" readonly="">
                <input type="datetime-local" name="video_open_at" id="video_open_at" class="d-none" value="{{ $request->get('open') ?? '' }}" readonly="">
                <input type="datetime-local" name="video_close_at" id="video_close_at" class="d-none" value="{{ $request->get('close') ?? '' }}" readonly="">
                <input type="type" class="form-control bg-white " aria-describedby="basic-addon1" name="file_name" id="file_name" value="{{ $details->m3u8file_path ?? '' }}" readonly="">
                  <div class="input-group-append bg-primary input-label">
                  <button type="button" onclick="$('#upload_file').click();" class="input-group-text bg-primary border-0" id="basic-addon1">ファイル選択</button>
                </div>
                  <div class="invalid-feedback" id="err_upload_file"></div>
              </div>
              <div class="col-md-1"></div>
              <div class="input-group col-md-2 mb-3">
                 <div class="input-group-prepend bg-white input-label">
                    <span class="input-group-text bg-white border-0" id="basic-addon1">開催日</span>
                  </div>
                 <input type="number" max="999.9" step="0.1" class="form-control bg-white " aria-describedby="basic-addon1" name="video_times" id="video_times"  value="{{ $details->video_times ?? '' }}">
                  <div class="input-group-append bg-white input-label">
                    <span class="input-group-text bg-white border-0" id="basic-addon1"> 分</span>
                  </div>
                  <div class="invalid-feedback" style="padding-left: 6em;" id="err_video_times"></div>
              </div>
            </div>
          </div>

           <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">

                <p class="m-1"><label for="flag">オリジナルファイルを保存する</label> <input type="checkbox" name="flag" id="flag" value="1" {{ (!empty($details->keep_original_flag) && $details->keep_original_flag == 1)? 'checked' : '' }} ></p>
                <p class="m-1">(オリジナルファイルを保存すると、ダウンロードが可能となります)</p>
              </div>
              <div class="col-md-6 text-right">
                <br><button type="submit" class="btn btn-primary px-4" id="submit_btn">アップロード </button>
              </div>
              <div class="col-md-12 text-right mt-2">
                  <a href="{{ route('SeminarVideo.details', [$seminar_id, $id]) }}" class="btn btn-warning px-4">戻る</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</form>
@endsection

@section('script')
<script type="text/javascript">
  $("#form_upload").on('submit', function(e){
    e.preventDefault();
    let form = $('#form_upload')[0];
    let data = new FormData(form);
    let url = $('#form_upload').attr('action');

    $.ajax({
        type:"POST",
        url:url,
        processData: false,
        contentType: false,
        cache: false,
        data:data,
        dataType:'json',
        beforeSend:function(){
          $("#submit_btn").prop('disabled', true);
        },
        success:function(response){
           // console.log(response);
          if (response.status === true) {
            // window.location = response.redirect;
            upload_video(response.redirect+'?');
          }else{
            showValidator(response.error, 'form_upload');
          }
          $("#submit_btn").prop('disabled', false);
        },
        error: function(error){
          $("#submit_btn").prop('disabled', false);
          console.log(error);
        }
      });
  });

  $("input[type=file]").on('change',function(){
    $("#file_name").val(this.files[0].name);
  });

</script>
@endsection



