@extends('Layout.account.app')
@section('title', '配信動画詳細')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/video.css') }}">
@endsection

@section('content')
<form id="submit_details_form" class="needs-validation" action="{{ route('SeminarVideo.save') }}" method="post" novalidate="">

  <input type="hidden" name="seminar_id" id="seminar_id" value="{{ $seminar_id ?? '' }}">
  <input type="hidden" name="id" id="id" value="{{ $id ?? '' }}">
	<div class="row justify-content-center mt-5">
    <div class="col-md-10 col-12">
        <div class="row justify-content-center">
        <!-- form label start -->
        <div class="col-sm-12">
          <label class="page-title">配信動画詳細</label>
        </div>
        <div class="col-12 col-md-12">
          <div class="row">
            <div class="col-md-3">
                <div class="input-group mb-3">
                  <div class="input-group-prepend bg-primary input-label">
                    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催日</span>
                  </div>
                  <input type="text" class="form-control bg-white col-md-5 text-center border-0" readonly="" aria-describedby="basic-addon1" value="{{ $details->seminar_date ?? '' }}">
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="input-group mb-3">
                  <div class="input-group-prepend bg-primary input-label">
                    <span class="input-group-text bg-primary border-0" id="basic-addon1">領域</span>
                  </div>
                  <input type="text" class="form-control bg-white text-center border-0" readonly="" aria-describedby="basic-addon1" value="{{ $details->category_no ?? '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group mb-3">
                  <div class="input-group-prepend bg-primary input-label">
                    <span class="input-group-text bg-primary border-0" id="basic-addon1">セミナー項目</span>
                  </div>
                  <input type="text" class="form-control bg-white border-0" readonly="" aria-describedby="basic-addon1" value="{{ $details->category_name ?? '' }}">
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="input-group mb-3">
            <div class="input-group-prepend bg-primary input-label">
              <span class="input-group-text bg-primary border-0" id="basic-addon1">配信開始</span>
            </div>
            <input type="datetime-local" class="form-control col-md-3" aria-describedby="basic-addon1" name="video_open_at" id="video_open_at" value="{{ $details->video->video_open_at ??  $request->get('open') }}">
            <div class="invalid-feedback" id="err_video_open_at"></div>
          </div>
          <div class="input-group mb-3">
            <div class="input-group-prepend bg-primary input-label">
              <span class="input-group-text bg-primary border-0" id="basic-addon1">配信終了</span>
            </div>
            <input type="datetime-local" class="form-control col-md-3" aria-describedby="basic-addon1" name="video_close_at" id="video_close_at" value="{{ $details->video->video_close_at ??  $request->get('close') }}">
            <div class="invalid-feedback" id="err_video_close_at"></div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
            <input type="hidden" class="form-control col-md-2" aria-describedby="basic-addon1" name="upload_file" id="upload_file" value="{{ $details->video->m3u8file_path ?? '' }}">
            <div class="invalid-feedback" id="err_upload_file"></div>
              @if(!empty($details->video->m3u8file_path))
                <P>ビデオアップロード済</P>
                <a href="{{ route('SeminarVideo.delete', [$id]) }}" class="btn btn-danger px-5">動画削除 </a>
              @endif
            </div>
            <div class="col-md-6 text-right">
              @if(!empty($details->video->m3u8file_path))
                <br>

                @if($details->video->keep_original_flag == 1)
                  <a href="{{ asset('video/'.$details->id.'/'.$details->video->m3u8file_path) }}" download="" class="btn btn-primary px-4">動画ダウンロード</a>
                @endif
              @else
                <br>
                <button type="button" onclick="upload_video('{{ route('SeminarVideo.upload', [$seminar_id, $id]) }}?');" class="btn btn-primary px-4">動画アップロード</button>
              @endif
            </div>
            <div class="col-md-12 text-right mt-2">
                <button type="submit" class="btn btn-primary px-4 mr-2">保存</button>
                <a href="{{ route('SeminarVideo.index') }}" class="btn btn-warning px-4">戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection

@section('script')

<script src="{{ asset('js/video.js?1') }}"></script>
<script src="{{ asset('js/hls.js?1') }}"></script>

<script type="text/javascript">
  $("#submit_details_form").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          window.location = response.redirect;
         }else{
          showValidator(response.error, 'submit_details_form');
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });

</script>
@endsection



