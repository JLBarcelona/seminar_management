@extends('Layout.account.app')
@section('title', '配信動画管理')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
    <div class="col-md-10 col-12">
        <div class="row justify-content-center">
        <!-- form label start -->
        <div class="col-sm-12">
          <label class="page-title">配信動画管理</label>
        </div>
        <div class="col-12 col-md-12">
          <table class="table dt-responsive table-bordered" id="tbl_seminar_video" style="width: 100%;"></table>
        </div>
        <div class="col-sm-12 text-right mt-2">
          <a href="{{ route('account.index') }}" class="btn btn-warning">戻る</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  show_seminar_video();

  function show_seminar_video(){
    $('#tbl_seminar_video').DataTable({
        pageLength: 10,
        responsive: true,
        ajax: {
          url: '{{ route('SeminarVideo.list') }}',
          error: function (xhr, error, code) {
              setTimeout(function(){
                show_seminar_video();
              },1500);
          }
        },
        searchable: false,
        "bLengthChange": false,
        "info": false,
        aaSorting: [],
        "bFilter": false,
        deferRender: false,
        language: {
        "emptyTable": "データなし",
        "paginate": {
          "previous": "前",
          "next": "次",
        }
      },
        columns: [{
        className: '',
        "data": "seminar_date",
        "title": "開催日",
      },{
        className: '',
        "data": "category_no",
        "title": "領域",
      },{
        className: '',
        "data": "category_name",
        "title": "セミナー項目",
      },{
        className: '',
        "data" : 'video',
        "title": "配信開始",
          "render": function(data, type, row, meta){
          return (row.video !== null) ? row.video.video_open_at : '';
        }
      },{
        className: '',
        "data" : 'video',
        "title": "配信終了",
          "render": function(data, type, row, meta){
          return (row.video !== null)? row.video.video_close_at : '';
        }
      },{
        className: 'width-option-1 text-center',
        "data": "id",
        "orderable": false,
        "title": "編集",
          "render": function(data, type, row, meta){
            newdata = '';
            let video_parameter = (row.video !== null)? '/'+row.video.id : '';
            newdata += '<a href="{{ route("SeminarVideo.details") }}/'+row.id+video_parameter+'" class="btn btn-primary btn-sm font-base mt-1" type="button">詳細</a> ';
            return newdata;
          }
        }
      ]
    });
  }
</script>
@endsection



