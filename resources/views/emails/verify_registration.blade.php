@component('mail::message')
<p>+Seminar メールアドレス確認</p><br>
<p>+Seminarへのユーザー登録を受け付けました。</p>
<p>下記URLよりメールアドレスの確認をお願い致します。</p><br>
<a href="{{$data['url']}}">{{$data['url']}}</a><br><br><br>
<p>─── お問い合わせ ───────────────────────────────── <br>
 {{$data['support_name']}}<br>
 {{$data['support_mail_address']}}<br>
</p>
