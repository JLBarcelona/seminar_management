@component('mail::message')
<div>

@if($data['email_type'] == "password_reset")

<p>下記URLよりパスワードの再設定をお願い致します。</p>

<a href="{{ $data['url'] }}"> {{$data['url']}} </a>

<p>このメールにお心あたりのない場合は、他の方が誤ってお客さまのメールアドレスを入力した可能性が
 ございますので、お見捨ておきください。</p>

<p>今後とも会社の+Seminarをよろしくお願いいたします。</p>

<p>─── お問い合わせ ───────────────────────────────── </p>

@endif
</div>

@endcomponent
