@extends('Layout.account.app')
@section('title', '講座情報')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row justify-content-center">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">講座情報</label>
				</div>
				<div class="col-md-12 col-12">
		          <div class="form-inline">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="checkbox_con">
                        <label class="form-check-label" for="checkbox_con">
                            開催済みの講座も表示する
                        </label>
                    </div>
		          </div>
              <a href="{{ route('seminar_exam.seminar_add') }}" class="btn btn-primary btn-sm col-sm-3 col-5 mr-n2 float-right"> 新規講座登録</a>
				</div>
        <div class="col-12">
          <table class="table dt-responsive table-bordered" id="tbl_seminar" style="width: 100%;"></table>
        </div>
        <div class="col-sm-12 text-right mt-2">
          <a href="{{ route('account.index') }}" class="btn btn-warning">戻る</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

  var tbl_seminar;
  function seminars(checkbox_con=false){

    if (tbl_seminar) {
      tbl_seminar.clear();
      tbl_seminar.destroy()
    }

    tbl_seminar = $('#tbl_seminar').DataTable({
      serverSide: true,
      processing: true,
      pageLength: 10,
      ajax: {
          url : '{{ route("seminar_exam.list") }}',
          data:{checkbox_con:checkbox_con},
          error: function (xhr, error, code) {
              //location.reload();
          }
       },
        searchable: false,
        "bLengthChange": false,
        "info": false,
        aaSorting: [],
        "bFilter": false,
        deferRender: false,
        language: {
        "emptyTable": "データなし",
        "paginate": {
          "previous": "前",
          "next": "次",
        }
      },
        columns: [{
        className: '',
        "orderable": false,
        "data": "seminar_date",
        "title": "開催日",
      },{
        className: '',
        "orderable": false,
        "data": "category_no",
        "title": "領域",
      },{
        className: '',
        "orderable": false,
        "data": "category_name",
        "title": "セミナー項目",
      },{
        className: '',
        "orderable": false,
        "data": "teacher_name",
        "title": "講師名",
      },{
        className: '',
        "orderable": false,
        "data": "teacher_affiliation1",
        "title": "講師肩書",
      },{
        width: '',
        className: 'text-center',
        "data": "id",
        "orderable": false,
        "title": "編集",
          "render": function(data, type, row, meta){
            newdata = '';
            newdata += '<a href="{{route('seminar_exam.seminar_detail','')}}/'+row.id+'" class="btn btn-primary btn-sm font-base mt-1 btn-block">詳細</a> ';
            return newdata;
          }
        },{
        width: '',
        className: 'text-center',
        "data": "id",
        "orderable": false,
        "title": "問題",
          "render": function(data, type, row, meta){
            newdata = '';
            newdata += '<a href="{{route('seminar_exam.show','')}}/'+row.id+'" class="btn btn-primary btn-sm font-base mt-1 btn-block">問題</a> ';
            return newdata;
          }
        }
      ]
    });
  }
  seminars();

  $("#checkbox_con").change(function(){
     seminars(this.checked);
  });

</script>
@endsection
