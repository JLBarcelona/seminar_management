@extends('Layout.account.app')
@section('title', '回答内容変更')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<!-- form label start -->
				<div class="col-md-12">
					<label class="page-title ">回答内容変更</label>
				</div>
        <div class="col-md-12 col-12">
           <div class="input-group mb-3 col-md-5 p-0 col-12">
            <div class="input-group-prepend bg-primary input-label">
              <span class="input-group-text bg-primary border-0" >氏名</span>
            </div>
            <input type="text"  class="form-control" name="fullname" id="fullname"  >
          </div>
				</div>
        <div class="col-md-4 col-7">
              <div class="input-group mb-3 p-0">
                <div class="input-group-prepend bg-primary input-label">
                  <span class="input-group-text bg-primary border-0" >開催日</span>
                </div>
                <input type="date"  class="form-control" name="date_from" id="date_from"  >
              </div>
        </div>
         <div class="col-md-6 col-3">
              <div class="input-group mb-3 p-0 col-5">
                <div class="input-group-prepend bg-white input-label">
                  <span class="input-group-text bg-white border-0 text-center px-2 mr-2" ><h3>~</h3></span>
                </div>
                <input type="date"  class="form-control" name="date_to" id="date_to"  >
              </div> 
        </div>
        <div class="col-md-12 text-right">
          <button type="button" class="btn btn-primary px-5" id="btn_search">検索</button>    
        </div>
        <div class="col-12 mt-3">
          <table class="table dt-responsive table-bordered" id="tbl_seminar" style="width: 100%;"></table>
        </div>
        <div class="col-sm-12 text-right mt-2">
          <a href="{{ route('account.index') }}" class="btn btn-warning ">戻る</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

  $('#btn_search').on('click', function(){
     seminars();
  });

  var tbl_seminar;
  function seminars(){
    let fullname = $('#fullname').val();
    let date_from = $('#date_from').val();
    let date_to = $('#date_to').val();
    if (tbl_seminar) {
      tbl_seminar.clear();
      tbl_seminar.destroy()
    }

    tbl_seminar = $('#tbl_seminar').DataTable({
      serverSide: true,
      processing: true,
      pageLength: 10,
      ajax: {
          url : '{{ route("seminar_exam.exam_list") }}',
          data:{fullname: fullname, date_from: date_from, date_to: date_to},
          error: function (xhr, error, code) {
              //location.reload();
          }
       },
        searchable: false,
        "bLengthChange": false,
        "info": false,
        aaSorting: [],
        "bFilter": false,
        deferRender: false,
        language: {
        "emptyTable": "データなし",
        "paginate": {
          "previous": "前",
          "next": "次",
        }
      },
        columns: [{
        className: '',
        "orderable": false,
        "data": "user_name",
        "title": "氏名",
      },{
        className: '',
        "orderable": false,
        "data": "seminar_date",
        "title": "開催日",
      },{
        className: '',
        "orderable": false,
        "data": "category_no",
        "title": "領域",
      },{
        className: '',
        "orderable": false,
        "data": "category_name",
        "title": "セミナー項目",
      },{
        className: '',
        "orderable": false,
        "data": "answer_1",
        "title": "回答１",
      },{
        className: '',
        "orderable": false,
        "data": "answer_2",
        "title": "回答２",
      },{
         className: '',
        "orderable": false,
        "data": "answer_3",
        "title": "回答３",
      },{
        width: '',
        className: 'text-center',
        "data": "seminar_registration_id",
        "orderable": false,
        "title": "変更",
          "render": function(data, type, row, meta){
            newdata = '';
            newdata += '<a href="{{route('seminar_exam.exam_list_detail','')}}/'+row.seminar_registration_id+'" class="btn btn-primary btn-sm font-base mt-1 btn-block">変更</a> ';
            return newdata;
          }
        }
      ]
    });
  }
  seminars();

</script>
@endsection
