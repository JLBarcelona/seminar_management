	@extends('Layout.account.app')
	@section('title', '講座詳細')
	@section('content')
		<div class="row justify-content-center mt-3">
			<div class="col-md-5 col-12">
				<form class="needs-validation" id="login_form" action="#" novalidate>
					@csrf
					<!-- input start -->
					<div class="row">
						<!-- form label start -->
						<div class="col-sm-12">
							<label class="page-title">講座詳細</label>
						</div>
						<!-- form label end -->
						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8" >
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">領域</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="category_no" id="category_no" aria-describedby="basic-addon1" value="{{$seminar->category_no}}" readonly="">
					    	  <div class="invalid-feedback" id="err_category_no"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">セミナー項目</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="category_name" id="category_name" aria-describedby="basic-addon1" value="{{$seminar->category_name}}" readonly="">
					    	  <div class="invalid-feedback" id="err_category_name"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">タイトル</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="seminar_title" id="seminar_title" aria-describedby="basic-addon1" value="{{$seminar->seminar_title}}" readonly="">
					    	  <div class="invalid-feedback" id="err_seminar_title"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">講師</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="teacher_name" id="teacher_name" aria-describedby="basic-addon1" value="{{$seminar->teacher_name}}" readonly="">
					    	  <div class="invalid-feedback" id="err_teacher_name"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">講師肩書</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="teacher_affiliation1" id="teacher_affiliation1" aria-describedby="basic-addon1" value="{{$seminar->teacher_affiliation1}}" readonly="">
					    	  <div class="invalid-feedback" id="err_teacher_affiliation1"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">会場</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="place" id="place" aria-describedby="basic-addon1" value="{{$seminar->place}}" readonly="">
					    	  <div class="invalid-feedback" id="err_place"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">定員</span>
							  </div>
							  <input type="text" class="form-control bg-white text-right" name="capacity" id="capacity" aria-describedby="basic-addon1" value="{{$seminar->capacity}}" readonly="">
					    	  <div class="invalid-feedback" id="err_capacity"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-8 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">申込受付開始</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="registration_from" id="registration_from" aria-describedby="basic-addon1" value="{{ date('Y-m-d H:i', strtotime($seminar->seminarGroup->registration_from)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_registration_from"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-8 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">申込受付終了</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="registration_to" id="registration_to" aria-describedby="basic-addon1" value="{{ date('Y-m-d H:i', strtotime($seminar->seminarGroup->registration_to)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_registration_to"></div>
							</div>
						</div>



						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催年月日</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="seminar_date" id="seminar_date" aria-describedby="basic-addon1" value="{{$seminar->seminar_date}}" readonly="">
					    	  <div class="invalid-feedback" id="err_seminar_date"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催時間</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="start_at" id="start_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->start_at)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_start_at"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催終了時間</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="end_at" id="end_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->end_at)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_login_id"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">当日受付開始時間</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="reception_open_at" id="reception_open_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->reception_open_at)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_reception_open_at"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" id="basic-addon1">受付終了時間</span>
							  </div>
							  <input type="text" class="form-control bg-white" name="reception_close_at" id="reception_close_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->reception_close_at)) }}" readonly="">
					    	  <div class="invalid-feedback" id="err_reception_close_at"></div>
							</div>
						</div>

					</div>
					<!-- input end -->
					<!-- button start -->
					<div class="row justify-content-end mb-2">
						<div class="col-sm-4 text-right mb-2">
							<a href="{{ route('seminar_exam.seminar_edit', ['seminar_id' => $seminar->id] )}}" class="btn btn-primary px-5 col-12 text-light">編集</a>
						</div>
						<div class="col-sm-4 text-right mb-2">
							<a href="{{ route('seminar_exam.index')}}" class="btn btn-warning px-5 col-12 text-dark">戻る</a>
						</div>

					</div>
				</div>
					<!-- button end -->
				</form>
			</div>
		</div>
	@endsection
	@section('script')


	<script type="text/javascript">

	</script>

	@endsection

