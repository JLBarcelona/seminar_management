@extends('Layout.account.app')
@section('title', '問題設定')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-6 col-12">
        <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">問題設定</label>
				</div>

				<div class="col-md-12 col-12">
              <label>{{ $seminar->seminar_date }}</label> <br>
              <label>{{ $seminar->category_no }} {{ $seminar->category_name }}</label>
				</div>
         <form id="exam_form">
            <div class="col-md-12 col-12 justify-content-center mt-4">
                @csrf
                <input type="hidden" name="seminar_id" value="{{ $seminar->id }}">
                <div class="row">

                    @if(!$seminar->exams->isEmpty())  
                      @foreach($seminar->exams as $key => $eq)
                          <input type="hidden" name="question_no[{{$eq->id}}]">
                          <div class="col-12 mb-2">
                            <div class="row">

                              <div class="col-3">問題{{ $key+=1 }}</div>

                              <div class="col-3"> 
                                <select class="form-control" name="answer[{{$eq->id}}]">
                                  <option value="0" {{ $eq->answer==0 ? 'selected' : '' }}>いいえ</option>
                                  <option value="1" {{ $eq->answer==1 ? 'selected' : '' }}>はい</option>
                                </select>
                              </div>

                              <div class="col-3">
                                   <label><span class="mr-3">講座</span> <input type="checkbox" name="seminar_use[{{$eq->id}}]" {{ $eq->seminar_use==0 ? '' : 'checked' }}></label>
                              </div>

                            </div>

                          <div class="row justify-content-end mt-2 mb-2">
                              <div class="col-9">
                                <textarea class="form-control" rows="2" name="exam_question[{{$eq->id}}]">{{ $eq->exam_question }}</textarea>
                              </div>
                            </div> 
                          </div>  


                      @endforeach
                    @else
                        
                    @endif


                    <div class="col-md-12 text-center mt-2 p-2">
                        <h5 class="error-div text-danger mb-5" style="display:none;"></h5>
                    </div>  
                          

                    <div class="col-md-12 text-right mt-2 p-2">
                     
                      <button type="submit" class="btn btn-primary">保存</button>
                       <a href="{{ route('seminar_exam.index') }}" class="btn btn-warning">戻る</a>
                    </div>  
                    
                </div>
                
            </div>
          </form>

      
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">


$("#exam_form").on('submit', function(e){
   e.preventDefault(e);

    const data = $('#exam_form').serialize();

    $('.error-div').hide();

    $.ajax({
        type:"POST",
        url: "{{ route('seminar_exam.store') }}",
        data:data,
        cache:false,
        beforeSend:function(){
            $(this).prop('disabled', true);
        },
        success:function(response){
          
          $(this).prop('disabled', false);
        
          if(response.status == true){
            window.location = response.redirect;
          }else{
            $('.error-div').text(response.message);
            $('.error-div').show();
          }           
      },
      error:function(error){
          console.log(error);
          $(this).prop('disabled', false);
      }
  });
});


</script>
@endsection
