@extends('Layout.app')
	@section('title', '講座新規登録')
	@section('content')
		<div class="row justify-content-center mt-3">
			<div class="col-md-5 col-12">
				<form id="seminar_form">
					@csrf
					<!-- input start -->
					<div class="row">
						<!-- form label start -->
						<div class="col-sm-12">
							<label class="page-title">講座新規登録</label>
						</div>
						<!-- form label end -->
						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >領域</span>
							  </div>
							  <input type="number" min="0" class="form-control" name="category_no" id="category_no"  value="" >
					    	  <div class="invalid-feedback" id="err_category_no"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >セミナー項目</span>
							  </div>
							  <input type="text" class="form-control " name="category_name" id="category_name"  value="" >
					    	  <div class="invalid-feedback" id="err_category_name"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >タイトル</span>
							  </div>
							  <input type="text" class="form-control " name="seminar_title" id="seminar_title"  value="" >
					    	  <div class="invalid-feedback" id="err_seminar_title"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >講師</span>
							  </div>
							  <input type="text" class="form-control " name="teacher_name" id="teacher_name"  value="" >
					    	  <div class="invalid-feedback" id="err_teacher_name"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >講師肩書</span>
							  </div>
							  <input type="text" class="form-control " name="teacher_affiliation1" id="teacher_affiliation1"  value="" >
					    	  <div class="invalid-feedback" id="err_teacher_affiliation1"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >会場</span>
							  </div>
							  <input type="text" class="form-control " name="place" id="place"  value="" >
					    	  <div class="invalid-feedback" id="err_place"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >定員</span>
							  </div>
							  <input type="number" min="0" class="form-control text-right" name="capacity" id="capacity"  value="">
					    	  <div class="invalid-feedback" id="err_capacity"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-8 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >申込受付開始</span>
							  </div>
							  <input type="datetime-local" class="form-control " name="registration_from" id="registration_from"  value="" >
					    	  <div class="invalid-feedback" id="err_registration_from"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-8 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >申込受付終了</span>
							  </div>
							  <input type="datetime-local" class="form-control " name="registration_to" id="registration_to"  value="" >
					    	  <div class="invalid-feedback" id="err_registration_to"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >開催年月日</span>
							  </div>
							  <input type="date" class="form-control " name="seminar_date" id="seminar_date"  value="" >
					    	  <div class="invalid-feedback" id="err_seminar_date"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >開催時間</span>
							  </div>

							  <input type="time" class="form-control " name="start_at" id="start_at"  value="" >
					    	  <div class="invalid-feedback" id="err_start_at"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >開催終了時間</span>
							  </div>
							  <input type="time" class="form-control " name="end_at" id="end_at"  value="" >
					    	  <div class="invalid-feedback" id="err_end_at"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >当日受付開始時間</span>
							  </div>
							  <input type="time" class="form-control " name="reception_open_at" id="reception_open_at"  value="" >
					    	  <div class="invalid-feedback" id="err_reception_open_at"></div>
							</div>
						</div>

						<div class="col-sm-12 col-12">
							<div class="input-group mb-3 col-md-6 p-0 col-8">
							  <div class="input-group-prepend bg-primary input-label">
							    <span class="input-group-text bg-primary border-0" >受付終了時間</span>
							  </div>
							  <input type="time" class="form-control " name="reception_close_at" id="reception_close_at" value="" >
					    	  <div class="invalid-feedback" id="err_reception_close_at"></div>
							</div>
						</div>

					</div>

					<div class="col-md-12 text-center mt-2 p-2">
                        <h5 class="error-div text-danger mb-5" style="display:none;"></h5>
                    </div>

					<div class="row justify-content-end mb-2">
						<div class="col-sm-4 text-right mb-2">
							<button type="submit" class="btn btn-primary px-5 text-light">登録</button>
						</div>
						<div class="col-sm-4 text-right mb-2">
							<a href="{{ route('seminar_exam.index') }}" class="btn btn-warning px-5 col-12 text-dark">戻る</a>
						</div>

					</div>
				</div>
					<!-- button end -->
				</form>
			</div>
		</div>
	@endsection
	@section('script')


	<script type="text/javascript">


		$("#seminar_form").on('submit', function(e){
			 e.preventDefault(e);
			var url = $(this).attr('action');
		    var mydata = $(this).serialize();

		 	$('.error-div').hide();

		    $.ajax({
			    	type:"POST",
			        url: "{{ route('seminar_exam.seminar_store') }}",
			        data:mydata,
			        cache:false,
			        beforeSend:function(){
			           $("#submit_button").prop('disabled', true);
			        },
			    	success:function(response){
			    	console.log(response);
			        if(response.status){
						window.location = response.redirect;
			        }else{
			            showValidator(response.error,'reset_password_form');
			            if(response.type && response.type=='dberror'){
							$('.error-div').text('保存に失敗しました '+'<br>'+response.error);
            				$('.error-div').show();
			            }

			        }
			        $("#submit_button").prop('disabled', false);
			    },
			    error:function(error){
					$("#submit_button").prop('disabled', false);
			    }
			});
		});


	</script>

	@endsection
