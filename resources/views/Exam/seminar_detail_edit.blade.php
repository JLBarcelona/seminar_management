@extends('Layout.account.app')
@section('title', '講座情報編集')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="seminar_detail_form" action="{{ route('seminar_exam.seminar_edit_save', $seminar->id) }}" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">講座情報編集</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">領域</span>
						  </div>
						  <input type="number" class="form-control bg-white" name="category_no" id="category_no" aria-describedby="basic-addon1" value="{{$seminar->category_no}}" >
				    	  <div class="invalid-feedback" id="err_category_no"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">セミナー項目</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="category_name" id="category_name" aria-describedby="basic-addon1" value="{{$seminar->category_name}}" >
				    	  <div class="invalid-feedback" id="err_category_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">タイトル</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="seminar_title" id="seminar_title" aria-describedby="basic-addon1" value="{{$seminar->seminar_title}}" >
				    	  <div class="invalid-feedback" id="err_seminar_title"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">講師</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="teacher_name" id="teacher_name" aria-describedby="basic-addon1" value="{{$seminar->teacher_name}}" >
				    	  <div class="invalid-feedback" id="err_teacher_name"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">講師肩書</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="teacher_affiliation1" id="teacher_affiliation1" aria-describedby="basic-addon1" value="{{$seminar->teacher_affiliation1}}" >
				    	  <div class="invalid-feedback" id="err_teacher_affiliation1"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">会場</span>
						  </div>
						  <input type="text" class="form-control bg-white" name="place" id="place" aria-describedby="basic-addon1" value="{{$seminar->place}}" >
				    	  <div class="invalid-feedback" id="err_place"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">定員</span>
						  </div>
						  <input type="number" class="form-control bg-white text-right" name="capacity" id="capacity" aria-describedby="basic-addon1" value="{{$seminar->capacity}}" >
				    	  <div class="invalid-feedback" id="err_capacity"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-8 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">申込受付開始</span>
						  </div>
						  <input type="datetime-local" class="form-control bg-white" name="registration_from" id="registration_from" aria-describedby="basic-addon1" value="{{$seminar->seminarGroup->registration_from}}" >
				    	  <div class="invalid-feedback" id="err_registration_from"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-8 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">申込受付終了</span>
						  </div>
						  <input type="datetime-local" class="form-control bg-white" name="registration_to" id="registration_to" aria-describedby="basic-addon1" value="{{$seminar->seminarGroup->registration_to}}" >
				    	  <div class="invalid-feedback" id="err_registration_to"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催年月日</span>
						  </div>
						  <input type="date" class="form-control bg-white" name="seminar_date" id="seminar_date" aria-describedby="basic-addon1" value="{{$seminar->seminar_date}}" >
				    	  <div class="invalid-feedback" id="err_seminar_date"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催時間</span>
						  </div>
						  <input type="time" class="form-control bg-white" name="start_at" id="start_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->start_at)) }}" >
				    	  <div class="invalid-feedback" id="err_start_at"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">開催終了時間</span>
						  </div>
						  <input type="time" class="form-control bg-white" name="end_at" id="end_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->end_at)) }}" >
				    	  <div class="invalid-feedback" id="err_end_at"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">当日受付開始時間</span>
						  </div>
						  <input type="time" class="form-control bg-white" name="reception_open_at" id="reception_open_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->reception_open_at)) }}" >
				    	  <div class="invalid-feedback" id="err_reception_open_at"></div>
						</div>
					</div>

					<div class="col-sm-12 col-12">
						<div class="input-group mb-3 col-md-6 p-0 col-8">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">受付終了時間</span>
						  </div>
						  <input type="time" class="form-control bg-white" name="reception_close_at" id="reception_close_at" aria-describedby="basic-addon1" value="{{ date('H:i', strtotime($seminar->reception_close_at)) }}" >
				    	  <div class="invalid-feedback" id="err_reception_close_at"></div>
						</div>
					</div>

				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end mb-2">
					<div class="col-sm-4 text-right mb-2">
						<button type="submit" class="btn btn-primary px-5 col-12">保存</button>
					</div>
					<div class="col-sm-4 text-right mb-2">
						<a href="{{ route('seminar_exam.seminar_detail', $seminar->id)}}" class="btn btn-warning px-5 col-12 text-dark">戻る</a>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
@endsection


@section('script')

<script type="text/javascript">
	$('#seminar_detail_form').on('submit', function(e){
		e.preventDefault();
		let formData = $(this).serialize();
		let  url = $(this).attr('action');
		$. ajax({
			type:'post',
			url:url,
			data:formData,
			dataType:'json',
			beforeSend:function(){
				$('#submit_btn').prop('disabled', true);
			},success:function(response){
				$('#submit_btn').prop('disabled', false);
				if (response.status === true) {
					console.log(response);
					window.location = response.redirect;
				}else{
					showValidator(response.error, 'seminar_detail_form');
				}
			},error:function(err){
				$('#submit_btn').prop('disabled', false);
					console.log(err);
			}
		});

	});
</script>

@endsection
