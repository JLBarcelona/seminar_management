<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition">
		<div class="">
			@include('Layout.nav', ['is_disabled' => (isset($is_disabled) && $is_disabled == true)? $is_disabled : false])
			<div class="bg-white">
			 	@yield('breadcrumbs')
			 	 <section class="content">
					 <div class="container-fluid">
						@yield('content')
					</div>
				 </section>
			</div>
			  <footer class="main-footer">
			   
			  </footer>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>