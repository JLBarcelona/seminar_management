<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition">
		<div class="">
			@include('Layout.account.nav', ['is_disabled' => (isset($is_disabled) && $is_disabled == true)? $is_disabled : false])
			<div class="container-fluid">
			  <div class="row justify-content-center">
			    <div class="col-sm-12 text-end">
			       <a href="{{ route('logout') }}" class="btn btn-outline-primary float-right">ログアウト</a>
			       <a href="{{ route('account.index') }}" class="btn btn-outline-primary float-right mr-2">メニューへ戻る</a>
			    </div>
			  </div>
			</div>
			<div class="bg-white">
			 	@yield('breadcrumbs')
			 	 <section class="content">
					 <div class="container-fluid">
						@yield('content')
					</div>
				 </section>
			</div>
			  <footer class="main-footer">
			  </footer>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>