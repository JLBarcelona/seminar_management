<nav class="main-header main-header-seminar navbar navbar-expand navbar-lightgray navbar-light row justify-content-start">
    
    <div class="p-3 bd-highlight"><img src="{{ asset('img/logo-transparent.png') }}" alt="Seminar Logo" class="brand-image" style=""></div>
    <div class="p-3 bd-highlight"><h3 class="text-primary mb-0 font-weight-bold">+Seminar</h3></div>
    <div class="p-3 bd-highlight"><h4 class="mb-0 font-weight-bold">管理機能</h4></div>

</nav>

<hr class="account-hr-border mt-0">

