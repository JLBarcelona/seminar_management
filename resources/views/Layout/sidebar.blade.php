<aside class="main-sidebar sidebar-dark-primary bg-primary elevation-4">
  <!-- Brand Logo -->
  @if(isset($is_disabled) && $is_disabled == true)
    <a class="brand-link" style="border-color: #a4b0d6;">
      <img src="{{ asset('img/logo.png') }}" alt="Miakata Logo" class="brand-image" >
      <span class="brand-text font-weight-light">&nbsp;</span>
    </a>
  @else
    <a class="brand-link" style="border-color: #a4b0d6;">
      <img src="{{ asset('img/logo.png') }}" alt="Miakata Logo" class="brand-image" >
      <span class="brand-text font-weight-light">&nbsp;</span>
    </a>
  @endif
  
  <!-- Sidebar -->
  <div class="sidebar p-0 fw-bold">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 justify-content-center text-center pl-0" style="border-color: #a4b0d6;">
      <div class="image d-none">
        
      </div>
      <div class="info pl-0">
        <a class="d-block">{{-- auth()->user()->user_information->full_name --}} LUM株式会社</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="sidebar side-item p-0">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->

        @if(isset($is_disabled) && $is_disabled == true)
          <li class="nav-item ">
            <a href="#" class="nav-link disable {{ (request()->is('account'))? 'active': '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                HOME
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link disable {{ (request()->is('account/request-management/*'))? 'active': '' }}">
              <i class="nav-icon fas fa-id-card"></i>
              <p>
                請求管理
              </p>
            </a>
          </li>
        @else
          <li class="nav-item ">
            <a href="{{ route('account.index') }}" class="nav-link {{ (request()->is('account'))? 'active': '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                HOME
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('account.request.management.index') }}" class="nav-link {{ (request()->is('account/request-management/*'))? 'active': '' }}">
              <i class="nav-icon fas fa-id-card"></i>
              <p>
                請求管理
              </p>
            </a>
          </li>
        @endif
        
        <!-- Sample Multi level -->
        <li class="nav-item d-none">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Layout Options
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right">6</span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="pages/layout/top-nav.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Top Navigation</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/layout/top-nav-sidebar.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Top Navigation + Sidebar</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
