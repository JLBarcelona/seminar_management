@extends('Layout.account.app')
@section('title', 'メール認証状況')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
    <div class="col-md-10 col-12">
        <div class="row justify-content-center">
        <!-- form label start -->
        <div class="col-sm-12">
          <label class="page-title">メール認証状況</label>
        </div>
        <div class="col-12 col-md-12">
          <table class="table dt-responsive table-bordered" id="tbl_users" style="width: 100%;"></table>
        </div>
        <div class="col-sm-12 text-right mt-2">
          <a href="{{ route('account.index') }}" class="btn btn-warning">戻る</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
  show_users();
  var tbl_users;
  function show_users(){
    if (tbl_users) {
      tbl_users.destroy();
    }

    tbl_users = $('#tbl_users').DataTable({
        pageLength: 10,
        responsive: true,
        ajax: {
          url: '{{ route('account.user.unverified.list') }}',
          error: function (xhr, error, code) {
              //location.reload();
              setTimeout(function(){
                show_users();
              },1500);
          }
        },
        searchable: false,
        "bLengthChange": false,
        "info": false,
        aaSorting: [],
        "bFilter": false,
        deferRender: false,
        language: {
        "emptyTable": "データなし",
        "paginate": {
          "previous": "前",
          "next": "次",
        }
      },
        columns: [{
        className: '',
        "data": "l_name",
        "title": "氏",
      },{
        className: '',
        "data": "f_name",
        "title": "名",
      },{
        className: '',
        "data": "mail_address",
        "title": "メールアドレス",
      },{
        className: '',
        "data": "hospital_name",
        "title": "病院",
      },{
        className: 'width-option-1 text-center',
        "data": "id",
        "orderable": false,
        "title": "認証済みにする",
          "render": function(data, type, row, meta){
            newdata = '';
            newdata += '<a href="{{ route('account.user.set.verify') }}/'+row.id+'" class="btn btn-primary btn-sm font-base mt-1" type="button">認証</a> ';
            return newdata;
          }
        }
      ]
    });
  }
  
</script>
@endsection



