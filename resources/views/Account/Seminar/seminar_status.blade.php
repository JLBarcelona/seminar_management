@extends('Layout.account.app')
@section('title', 'ステータス変更')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-md-10 col-12">
            <div class="row">
                <!-- form label start -->
                <div class="col-sm-12">
                    <label class="page-title">ステータス変更</label>
                </div>

                <form class="needs-validation" id="frm_search" action="{{ route('account.seminar.filter') }}" novalidate>
                @csrf

                    <div class="col-sm-12 col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend bg-primary input-label" style="min-width: 120px">
                                <span class="input-group-text bg-primary border-0" id="basic-addon5">会員番号</span>
                            </div>
                            <input type="text" class="form-control col-1 text-center" placeholder="ア" name="certificate_id1" maxlength="1" id="certificate_id1">
                            <div class="col-1 text-center">
                                <h3 class="mb-0">-</h3>
                            </div>
                            <input type="number" class="form-control col-2" name="certificate_id2" min="1" maxlength="4" placeholder="1234" id="certificate_id2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                        </div>
                    </div>

                    <div class="col-sm-12 col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend bg-primary input-label" style="min-width: 120px">
                                <span class="input-group-text bg-primary border-0" id="basic-addon5">氏名</span>
                            </div>
                            <input type="text" class="form-control col-5" name="user_fullname" id="user_fullname">
                        </div>
                    </div>

                    <div class="col-sm-12 col-12">
                        <div class="form-inline">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend bg-primary input-label" style="min-width: 120px">
                                    <span class="input-group-text bg-primary border-0" id="basic-addon5">QRコード</span>
                                </div>
                                <input type="text" class="form-control col-1" name="qr_1" id="qr_1"><h3 class="mb-0 mx-3">,</h3>
                                <input type="text" class="form-control col-1" name="qr_2" id="qr_2"><h3 class="mb-0 mx-3">,</h3>
                                <input type="text" class="form-control col-1" name="qr_3" id="qr_3"><h3 class="mb-0 mx-3">,</h3>
                                <input type="text" class="form-control col-1" name="qr_4" id="qr_4">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-danger mb-5" id="show_error"></h5>
                    </div> 
                    <div class="col-sm-12 col-12 text-right">
                        <button class="btn btn-primary" type="submit" id="btn_submit">保存</button>
                    </div>
                </form>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12 col-12">
                    <div class="table"> 
                        <table class="table table-bordered" id="tbl_registered"></table>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center mb-2 d-none" id="show_actions">
                <div class="col-sm-12 text-right mb-2">
                    <div class="row justify-content-center">

                        <div class="col-6 text-left"><p id="td1"></p></div>
                        <div class="col-6"><b><p id="user_status">未受付</p></b></div>
                        <div class="col-12 text-center"><p id="sem_cat"></p></div>
                        <input type="text" class="d-none" id="user_seminar_status_id" name="user_seminar_status_id"> 
                        <input type="text" class="d-none" id="seminar_registration_id" name="seminar_registration_id"> 

                        <!-- button start -->
                        <div class="col-2"></div>
                        <div class="col-4 text-center">
                            <button class="btn btn-primary w-100" onclick="update_reception('reception_in_at')" disabled="" id="btn_participate">参加受付</button>
                        </div>
                        <div class="col-4 text-center">
                            <button class="btn btn-primary w-100" onclick="update_reception('reception_out_at')" disabled="" id="btn_exit_confirmation">退席確認</button>
                        </div>
                        <div class="col-2"></div>
                        <!-- button end -->
                    </div>               
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-sm-12 text-right mt-2">
                  <a href="{{ route('account.index') }}" class="btn btn-warning">戻る</a>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    $("#frm_search").on('submit', function(e){
        var url = $(this).attr('action');
        var mydata = $(this).serialize();
        e.stopPropagation();
        e.preventDefault(e);

        $('#tbl_registered').html('');
        $('#show_actions').addClass('d-none');

        $.ajax({
                type:"POST",
                url:url,
                data:mydata,
                cache:false,
                beforeSend:function(){
                   $("#btn_submit").prop('disabled', true);
                },
                success:function(response){
                 $("#btn_submit").prop('disabled', false);
                if(response.status == true){
                    registration_data(response.data);
                    $('#show_error').text('');
                }else{
                    $('#show_error').text(response.message);
                    showValidator(response.error,'login_form');
                }
            },
            error:function(error){
                console.log(error);
                $("#btn_submit").prop('disabled', false);
            }
        });
    });

    function registration_data(data){

        let output = data.map(row => {
            var certificate_id = row.user.certificate_id1 + '-' + row.user.certificate_id2;
            var td1 = certificate_id + ' ' + row.user.l_name + ' ' + row.user.f_name;
            let out = '<tr>\
                           <td>'+td1+'</td>\
                           <td class="text-center">'+row.seminar.category_no+'</td>\
                           <td>'+row.seminar.category_name+'</td>\
                           <td><button data-id="'+row.id+'" data-td1="'+td1+'" data-categoryno="'+row.seminar.category_no+'" data-categoryname="'+row.seminar.category_name+'" data-userid = '+row.user.id+' class="btn btn-primary btn-block btn-sm" onclick="reg_info(this);">選択</button></td>\
                       </tr>';
            return out;
        }).join('');

        $('#tbl_registered').html(output);

    }

    function reg_info(_this){

        var id = $(_this).data('id'); //seminar_registration_id
        var td1 = $(_this).data('td1');
        var category_no = $(_this).data('categoryno');
        var category_name = $(_this).data('categoryname');
        var user_id = $(_this).data('userid');

        var url = '{{route('account.seminar.user.status','')}}'+'/'+id;

        $.ajax({
            type:"GET",
            url:url,
            data:{},
            dataType:'json',
            success:function(response){

                $('#show_actions').removeClass('d-none');
                $('#td1').text(td1);
                $('#sem_cat').text(category_no+' '+category_name);
                $('#seminar_registration_id').val(id);
                if (response.status == true) {

                    // display user status text
                    if((response.data == null || response.data == '')){
                        $('#user_status').text('未受付');
                    }else if(response.data.reception_in_at !== null && response.data.exam_answered_at === null){
                        $('#user_status').text('参加受付');
                    }else if(response.data.exam_answered_at !== null && response.data.reception_out_at === null){
                        $('#user_status').text('テスト解答');
                    }else if(response.data.reception_out_at !== null){
                        $('#user_status').text('退席確認');
                    }

                    if(response.data == null || response.data == ''){
                        $('#btn_participate').prop('disabled',false);
                        $('#user_seminar_status_id').val('');
                    }else{
                        $('#user_seminar_status_id').val(response.data.id);
                        //buttons
                        if(response.data.reception_in_at !== null){
                            $('#btn_participate').prop('disabled',true);
                        }else{
                            $('#btn_participate').prop('disabled',false);
                        }

                        if(response.data.reception_in_at === null){
                            $('#btn_exit_confirmation').prop('disabled',true);
                        }else if(response.data.reception_out_at !== null){
                            $('#btn_exit_confirmation').prop('disabled',true);
                        }else if(response.data.exam_answered_at !== null){
                            $('#btn_exit_confirmation').prop('disabled',false);
                        }
                    }
                    
                }else{
                    console.log(response);
                }
            },
            error: function(error){
              console.log(error);
            }
          });
    }

    function update_reception(type){
        var user_seminar_status_id = $('#user_seminar_status_id').val();
        var seminar_registration_id = $('#seminar_registration_id').val();
        var url = '{{route('account.seminar.user.seminar.reception.update')}}';
        $.ajax({
            type:"POST",
            url:url,
            data:{user_seminar_status_id:user_seminar_status_id,type:type,seminar_registration_id:seminar_registration_id},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                if(response.type == "reception_in_at"){
                    $('#btn_participate').prop('disabled',true);
                    $('#user_status').text('参加受付');
                    $('#user_seminar_status_id').val(response.user_seminar_status_id);
                }else if(response.type == "reception_out_at"){
                    $('#btn_exit_confirmation').prop('disabled',true);
                    $('#user_status').text('退席確認');
                    $('#user_seminar_status_id').val(response.user_seminar_status_id);
                }
             }else{
                location.reload();
              // console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
        });
    }

</script>

@endsection