@extends('Layout.account.app_no_logout')
@section('title', '問題の回答が完了していません')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1 class="mb-4  text-danger"><b>問題の回答が完了していません</b></h1>
          <h3>
            <p class="m-1 text-danger">退席確認前に問題の回答を完了させてください</p>
            <p class="mt-5"><i class="fa fa-exclamation text-danger fa-5x"></i></p>
          </h3>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  setTimeout(function(){
    window.location = '{{ route("account.seminar.qrcode", $id) }}';
  },4000);
</script>
@endsection
