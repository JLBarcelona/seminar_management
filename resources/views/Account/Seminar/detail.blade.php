@extends('Layout.account.app')
@section('title', '申込状況詳細')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
            <div class="row">
                <div class="col-sm-12">
                    <label>{{date('Y/m/d', strtotime($seminar->seminar_date))}}</label>
                    <div class="col-sm-12">
                        <label class="col-sm-6 col-3 ml-n3">{{$seminar->category_no}} {{$seminar->category_name}}</label>
                        <a href="{{ route('account.seminar.export.excel', $seminar->id) }}" class="btn btn-primary btn-sm col-sm-3 col-5 mr-n2 float-right">CSVダウンロード</a>
                    </div>
                </div>
                <div class="col-md-12 col-12 mt-2 mb-2">
                    <div class="form-inline">
                        </div>
                            <table class="table dt-responsive table-bordered" id="tbl_seminar_registration" style="width: 100%;"></table>
                        </div>
                        <div class="col-sm-12 text-right mt-2">
                            <a href="{{ route('account.seminar')."/".$date_from."/".$date_to }}" class="btn btn-warning">戻る</a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

  var tbl_seminar_registration;
  function seminars(){
    if (tbl_seminar_registration) {
      tbl_seminar_registration.clear();
      tbl_seminar_registration.destroy()
    }

    tbl_seminar_registration = $('#tbl_seminar_registration').DataTable({
    serverSide: true,
    processing: true,
    pageLength: 10,
    ajax: {
        url : '{{ route("account.seminar.detail.list",$seminar->id) }}',
        data:{},
        // error: function (xhr, error, code) {
        //      location.reload();
        // }
     },
      searchable: false,
      "bLengthChange": false,
      "info": false,
      aaSorting: [],
      "bFilter": false,
      deferRender: false,
      language: {
      "emptyTable": "データなし",
      "paginate": {
        "previous": "前",
        "next": "次",
      }
    },
      columns: [{
      className: '',
      "orderable": false,
      "data": "registration_at",
      "title": "申込日",
      "render": function(data, type, row, meta){
            data = new Date().toISOString().slice(0, 10);
            return data;
        }
    },{
      className: '',
      "orderable": false,
      "data": "l_name",
      "title": "氏",
    },{
      className: '',
      "orderable": false,
      "data": "f_name",
      "title": "名",
    },{
      className: '',
      "orderable": false,
      "data": "l_name_kana",
      "title": "氏（ヨミガナ）",
    },{
      className: '',
      "orderable": false,
      "data": "f_name_kana",
      "title": "名（ヨミガナ）",
    },{
      className: '',
      "orderable": false,
      "data": "certificate_id1",
      "title": "会員番号",
    },{
      className: '',
      "orderable": false,
      "data": "registration_id",
      "title": "参加証番号",
    },{
      className: '',
      "orderable": false,
      "data": "mail_address",
      "title": "Emailアドレス",
    },{
      className: '',
      "orderable": false,
      "data": "hospital_name",
      "title": "所属施設",
    },{
      className: '',
      "orderable": false,
      "data": "devision_name",
      "title": "所属部署",
    },{
      className: '',
      "orderable": false,
      "data": "tel",
      "title": "TEL",
    },{
      className: '',
      "orderable": false,
      "data": "birthday",
      "title": "生年月日",
    },{
      className: '',
      "orderable": false,
      "data": "question_1_answer",
      "title": "問題１"
    },{
      className: '',
      "orderable": false,
      "data": "question_1_correct",
      "title": "正誤１"
    },{
      className: '',
      "orderable": false,
      "data": "question_2_answer",
      "title": "問題２"
    },{
      className: '',
      "orderable": false,
      "data": "question_2_correct",
      "title": "正誤２"
    },{
      className: '',
      "orderable": false,
      "data": "question_3_answer",
      "title": "問題３"
    },{
      className: '',
      "orderable": false,
      "data": "question_3_correct",
      "title": "正誤３"
    },{
      className: '',
      "orderable": false,
      "data": "question_4_answer",
      "title": "問題４"
    },{
      className: '',
      "orderable": false,
      "data": "question_4_correct",
      "title": "正誤４"
    },{
      className: '',
      "orderable": false,
      "data": "question_5_answer",
      "title": "問題５"
    },{
      className: '',
      "orderable": false,
      "data": "question_5_correct",
      "title": "正誤５"
    },{
      className: '',
      "orderable": false,
      "data": "correct_answers",
      "title": "合否"
    }
    //check google_excel document -> add user_exams fields
    ]
    });
  }
  seminars();
</script>
@endsection
