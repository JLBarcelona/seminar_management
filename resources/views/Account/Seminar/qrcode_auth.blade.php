@extends('Layout.account.app_no_logout')
@section('title', 'パスワード確認')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-4 col-12">
      <form id="auth_password" class="needs-validation" action="{{ route('account.seminar.qrcode_auth_back') }}" novalidate="">
         <div class="card round-0">
          <div class="card-body">
            <div class="row">
              <!-- form label start -->
              <div class="col-sm-12">
                <label class="page-title">パスワード確認</label>
              </div>
              <div class="col-md-12 col-12 form-group">
                <label>管理者パスワードを入力してください。</label>
                <div class="input-group mb-3">
                  <input type="password" class="form-control" name="password" id="password" aria-describedby="basic-addon2" autocomplete="off">
                   <div class="input-group-append bg-primary input-label">
                    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
                  </div>
                  <div class="invalid-feedback" id="err_password"></div>
                </div>

              </div>
              <div class="col-md-12 col-12 text-right">
                <button type="submit" class="btn btn-primary">確認</button>
              </div>
            </div>
          </div>
        </div>
      </form>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  $("#auth_password").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let formData = $(this).serialize();

    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          window.location = '{{ $redirect }}';
         }else{
          showValidator(response.error, 'auth_password')
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });
</script>
@endsection
