@extends('Layout.account.app')
@section('title', '申し込み状況確認')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">申し込み状況確認</label>
				</div>
				<div class="col-md-12 col-12">
          <div class="form-inline">
              <div class="input-group mb-3">
                <div class="input-group-prepend bg-primary input-label" style="min-width: 80px;">
                  <span class="input-group-text bg-primary border-0" id="basic-addon1">期間</span>
                </div>
                <input type="date" class="form-control" name="date_from" id="date_from" aria-describedby="basic-addon1">
              </div>
              <div class="input-group mb-3 mx-2">
                <label class="h4">~</label>
              </div>
              <div class="input-group mb-3">
                <input type="date" class="form-control" name="date_to" id="date_to" aria-describedby="basic-addon2">
                <button class="btn btn-primary rounded-0" type="button" id="basic-addon2" onclick="seminars();" style="min-width: 80px;">検索</button>
              </div>
          </div>
					<table class="table dt-responsive table-bordered" id="tbl_seminar" style="width: 100%;"></table>
				</div>
        <div class="col-sm-12 text-right mt-2">
          <a href="{{ route('account.index') }}" class="btn btn-warning">戻る</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

  var tbl_seminar;
  function seminars(){
    if (tbl_seminar) {
      tbl_seminar.clear();
      tbl_seminar.destroy()
    }
    
    date_from = $('#date_from').val();
    date_to =  $('#date_to').val();

    tbl_seminar = $('#tbl_seminar').DataTable({
    serverSide: true,
    processing: true,
    pageLength: 10,
    ajax: {
        url : '{{ route("account.seminar.list") }}',
        data:{date_from:date_from,date_to:date_to},
        error: function (xhr, error, code) {
             location.reload();
        }
     },
      searchable: false,
      "bLengthChange": false,
      "info": false,
      aaSorting: [],
      "bFilter": false,
      deferRender: false,
      language: {
      "emptyTable": "データなし",
      "paginate": {
      "previous": "前",
      "next": "次",
    }
    },
      columns: [{
      className: '',
      "orderable": false,
      "data": "seminar_date",
      "title": "開催日",
    },{
      className: '',
      "orderable": false,
      "data": "category_no",
      "title": "領域",
    },{
      className: '',
      "orderable": false,
      "data": "category_name",
      "title": "セミナー項目",
    },{
      width: '10%',
      className: 'text-right',
      "orderable": false,
      "data": "capacity",
      "title": "定員",
    },{
      width: '10%',
      className: 'text-right',
      "orderable": false,
      "data": "seminar_registration_count",
      "title": "申込済",
    },{
      width: '',
      className: 'text-center',
      "data": "id",
      "orderable": false,
      "title": "詳細",
      "render": function(data, type, row, meta){
        newdata = '';
        if(date_from.length > 0 && date_to.length == 0){
             newdata += '<a href="{{route('account.seminar.detail.get')}}/'+row.id+'/'+date_from+'/empty" class="btn btn-primary btn-sm font-base mt-1 btn-block">詳細</a> ';
        }else if(date_from.length == 0 && date_to.length > 0){
            newdata += '<a href="{{route('account.seminar.detail.get')}}/'+row.id+'/empty/'+date_to+'" class="btn btn-primary btn-sm font-base mt-1 btn-block">詳細</a> ';
        }else{
              newdata += '<a href="{{route('account.seminar.detail.get')}}/'+row.id+'/'+date_from+'/'+date_to+'" class="btn btn-primary btn-sm font-base mt-1 btn-block">詳細</a> ';
        }
        return newdata;
      }
      }
    ]
    });
  }

  $('#date_from').val(@js($date_from));
  $('#date_to').val(@js($date_to));
  seminars();
  
</script>
@endsection
