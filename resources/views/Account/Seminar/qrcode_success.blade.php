@extends('Layout.account.app_no_logout')
@section('title', $message)

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1 class="mb-4"><b>{{ $message }}</b></h1>
          <h3>
            <p class="m-1">スマートフォンの画面が</p>
            <p class="m-1"><b>{{ $status_message }}</b></p>
            <p class="m-1">になっている事を確認してください</p>
          </h3>
          <img src="{{ asset('img/page'.$page_image.'.PNG') }}" class="img-fluid mb-3">
				</div>
        <div class="col-sm-12 text-right">
          <a href="{{ route('account.seminar.qrcode_auth', 'status-change') }}" class="btn btn-primary">手動操作</a>
          <a href="{{ route('account.seminar.qrcode_auth', 'main-menu') }}" class="btn btn-warning">メニュー</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  setTimeout(function(){
    window.location = '{{ route("account.seminar.qrcode", $id) }}';
  },4000);
</script>
@endsection
