@extends('Layout.account.app_no_logout')
@section('title', 'セミナーを選択してください')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1 class="mb-4  text-danger"><b>セミナーを選択してください</b></h1>
          <div class="row justify-content-center">
            <div class="col-sm-6">
               @foreach($seminars as $seminar)
                <a href="{{ route('account.seminar.qrcode', $seminar->id) }}" class="btn btn-primary col-sm-12 my-2">領域{{ $seminar->category_no }} {{ $seminar->category_name }}</a>
              @endforeach
            </div>
          </div>
				</div>
        <div class="col-sm-12 text-right">
          <a href="{{ route('account.seminar.qrcode_auth', 'status-change') }}" class="btn btn-primary">手動操作</a>
          <a href="{{ route('account.seminar.qrcode_auth', 'main-menu') }}" class="btn btn-warning">メニュー</a>
        </div>
			</div>
		</div>
	</div>
@endsection

