@extends('Layout.account.app_no_logout')
@section('title', 'QRコードが一致しません')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1 class="mb-4  text-danger"><b>QRコードが一致しません</b></h1>
          <h3>
            <p class="m-1 text-danger"><< QRコード読み込み可能なセミナー >></p>
            <p class="m-1 mb-3 text-danger"><b>近くのスタッフにお声がけください</b></p>
            <p class="mt-5"><i class="fa fa-exclamation text-danger fa-5x"></i></p>
          </h3>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  setTimeout(function(){
    window.location = '{{ route("account.seminar.qrcode", $id) }}';
  },3000);
</script>
@endsection
