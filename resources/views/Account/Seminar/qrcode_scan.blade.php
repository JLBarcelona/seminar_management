@extends('Layout.account.app_no_logout')
@section('title', 'QRコードの読み込みを行なってください')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1>QRコードの読み込みを行なってください</h1>
          <h2 class="text-primary"><strong><small>{{ $seminars->seminarGroup->group_name }}</small></strong></h2>
          <form id="qr_code_form" action="{{ route('account.seminar.qrcode_scan') }}" method="POST" novalidate>
            <div class="form-group text-center">
              <label id="scan_label_loader" class="text-danger"></label>
              <input type="hidden" name="seminar_id" id="seminar_id" value="{{ $seminars->id }}">
              <input type="text" name="qr_code" id="qr_code" class="form-control text-center" style="opacity: 0; cursor: default;" placeholder="Enter the qrcode for simulation..." autofocus="">
              <img src="{{ asset('img/page1.PNG') }}" class="img-fluid mb-3">
            </div>
            <!-- <button class="btn btn-primary" type="submit">Simulate Scanner</button> -->
          </form>
				</div>

        <div class="col-sm-12 text-right">
          <a href="{{ route('account.seminar.qrcode_auth', 'status-change') }}" class="btn btn-primary">手動操作</a>
          <a href="{{ route('account.seminar.qrcode_auth', 'main-menu') }}" class="btn btn-warning">メニュー</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  // $(document).ready(function(){
  //   var env_counter = 0;
  //   setInterval(function(){
  //     env_counter++;
  //     qr_check_environment(env_counter);      
  //   }, 1500);

  //   function qr_check_environment(count){
  //     if (count === 10) {
  //         $("#scan_label_loader").text('Place the QRCode Properly!');
  //     }else if (count === 20) {
  //       window.location = '{{ route('account.seminar.qrcode_status_error') }}';
  //     }
  //   }
  // });

  $("#qr_code_form").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
          $("#scan_label_loader").text('Scanning...');
        },
        success:function(response){
          // console.log(response);
          if (response.status === true) {
            window.location = response.redirect;
          }else{
            $("#scan_label_loader").text(response.message);     
          }
           setTimeout(function(){
              $("#scan_label_loader").text('');  
            },2000);
        },
        error: function(error){
          $("#scan_label_loader").text('Error!');
          console.log(error);
           setTimeout(function(){
            $("#scan_label_loader").text('');  
          },2000);
        }
      });
  });

  $("html").on('click', function(){
    $("#qr_code").focus();
  });
</script>
@endsection
