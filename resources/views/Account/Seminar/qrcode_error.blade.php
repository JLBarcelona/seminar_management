@extends('Layout.account.app_no_logout')
@section('title', 'QRコードの読み込みに失敗しました')

@section('css')
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-10 col-12">
        <div class="row">
				<div class="col-sm-12 text-center">
				  <h1 class="mb-4  text-danger"><b>QRコードの読み込みに失敗しました</b></h1>
          <h3>
            <p class="m-1 text-danger">QRコードが正常に読み込まれませんでした</p>
            <p class="m-1 mb-3 text-danger"><b>近くのスタッフにお声がけください</b></p>
            <p class="mt-5"><i class="fa fa-exclamation text-danger fa-5x"></i></p>
          </h3>
				</div>
        <div class="col-sm-12 text-right">
          <a href="{{ route('account.seminar.qrcode_auth', 'status-change') }}" class="btn btn-primary">手動操作</a>
          <a href="{{ route('account.seminar.qrcode_auth', 'main-menu') }}" class="btn btn-warning">メニュー</a>
        </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
  setTimeout(function(){
    window.location = '{{ route("account.seminar.qrcode", $id) }}';
  },4000);
</script>
@endsection
