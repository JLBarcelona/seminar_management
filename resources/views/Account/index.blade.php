@extends('Layout.account.app')
@section('title', 'メニュー')

@section('css')
<style type="text/css">

</style>
@endsection

@section('content')
	<div class="row justify-content-center mt-5">
		<div class="col-md-5 col-12">
            <div class="row">
				<!-- form label start -->
				<div class="col-sm-12">
					<label class="page-title">メニュー</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-6 text-center">
					<label>【講座当日】</label>
					<!-- QRコード読み込み -->
					<div class="row justify-content-center mb-2">
							<a href="{{ route('account.seminar.today') }}" class="btn btn-primary btn-block col-md-8">QRコード読み込み</a>
							<a href="{{ route('account.seminar.status') }}" class="btn btn-primary btn-block col-md-8">ステータス変更</a>
					</div>
					<label>【状況確認】</label>
					<div class="row justify-content-center">
							<a href="{{ route('account.seminar') }}" class="btn btn-primary btn-block col-md-8">申込状況確認</a>
							<a href="#" class="btn btn-secondary btn-block col-md-8">回答状況確認</a>
							<a href="{{ route('seminar_exam.change_exam') }}" class="btn btn-primary btn-block col-md-8">回答内容変更</a>
					</div>
				</div>

				<div class="col-md-6 col-6 text-center">
					<label>【講座情報】</label>
					<div class="row justify-content-center mb-2">
							<a href="{{ route('seminar_exam.index') }}" class="btn btn-primary btn-block col-md-8">講座情報</a>
							<a href="{{ route('SeminarVideo.index') }}" class="btn btn-primary btn-block col-md-8">配信ビデオ管理</a>
					</div>


					<label>【ユーザー管理】</label>
					<div class="row justify-content-center mb-2">
							<a href="{{ route('account.user.unverified') }}" class="btn btn-primary btn-block col-md-8">メール認証状況</a>
					</div>
				</div>
				<div class="col-md-6 col-6 text-center"></div>
			</div>
		</div>
	</div>
@endsection
