@extends('Layout.app')
@section('title', 'Login')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12 text-center">
						<label class="page-title">メールアドレスの認証が完了しました</label>
					</div>
					<!-- form label end -->
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end">
					<div class="col-sm-12 text-center">
						<a href="{{ route('login') }}" class="btn btn-outline-primary">ログイン画面</a>
					</div>
				</div>
				<!-- button end -->
		</div>
	</div>
@endsection
