@extends('Layout.app')
@section('title', 'Registration Success')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<!-- input start -->
			<div class="row p-4">
				<div class="col-sm-12 col-12 text-center">
					<p class="fs-4 fw-bold"><strong>入力されたメールアドレス宛に確認メールを送信しました。<br> 確認メールに記載のURLから登録認証を完了してください。</strong></p>
				</div>
			</div>
		</div>
	</div>
@endsection