@extends('Layout.app')
@section('title', 'パスワードリセット')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="forget_password_form" action="{{ route('auth.forget.password.post') }}" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">パスワードリセット</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">Emailアドレス</span>
						  </div>
						  <input type="text" class="form-control" name="mail_address" id="mail_address" aria-describedby="basic-addon1">
				    	  <div class="invalid-feedback" id="err_mail_address"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end">
					<div class="col-sm-5 col-10 text-right">
						<button type="submit" class="btn btn-outline-primary px-5">パスワードリセット</button>
					</div>
					<!-- <div class="col-sm-12 text-right">
						<a href="" class="btn btn-link text-dark text-sm">＊パスワードを忘れた方はこちら</a>
					</div> -->
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

	$("#forget_password_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		        if(response.status){
					$('#forget_password_form')[0].reset();
					window.location.replace(response.url+'?email='+response.mail_address);
		        }else{
		            showValidator(response.error,'forget_password_form');
		        }
		        $("#submit_button").prop('disabled', false);
		    },
		    error:function(error){
				$("#submit_button").prop('disabled', false);
		    }
		});
	});

</script>
@endsection