@extends('Layout.app')
@section('title', 'パスワード再設定')

@section('css')
<style type="text/css">
	
</style>
@endsection

@section('content')
    @if ($user_data)
    <div class="row justify-content-center">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="reset_password_form" action="{{ route('auth.reset.password.post') }}" novalidate>
				@csrf
				<input type="hidden" name="id" value="{{$user_data->id}}">
                <input type="hidden" name="mail_address" value="{{$user_data->mail_address}}">
                <input type="hidden"  name="password_reset_parameter" value="{{$user_data->password_reset_parameter}}">
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">パスワード再設定</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label" style="min-width: 185px;">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">新しいパスワード</span>
						  </div>
						  <input type="password" placeholder="英数字8桁（半角英字、数字）" min="8" max="8" maxlength="8" name="password" id="password" class="form-control">
						  <div class="input-group-append bg-primary input-label">
						    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
						  </div>
				    	  <div class="invalid-feedback" id="err_password"></div>
						</div>
					</div>
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label" style="min-width: 185px;">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">新しいパスワード確認</span>
						  </div>
						  <input type="password" placeholder="英数字8桁（半角英字、数字）" min="8" max="8" maxlength="8" name="password_confirmation" id="password_confirmation" class="form-control">
						  <div class="input-group-append bg-primary input-label">
						    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password_confirmation');"></i></span>
						  </div>
				    	  <div class="invalid-feedback" id="err_password_confirmation"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end">
					<div class="col-sm-5 col-5 text-right">
						<button class="btn btn-outline-primary" id="submit_button" type="submit">変更</button>
					</div>
					<!-- <div class="col-sm-12 text-right">
						<a href="" class="btn btn-link text-dark text-sm">＊パスワードを忘れた方はこちら</a>
					</div> -->
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>

    @else

        <div class="row justify-content-center">
        	<div class="col-sm-12">
				<label class="page-title">パスワード再設定</label>
			</div>
			<div class="col-sm-12 col-12 text-center">
		        <p class="fs-4 text-warning"><strong>このＵＲＬは無効となっております。 <br> 新たにお送りしたＵＲＬから、もう一度アクセスしてください。 </strong></p>
		        <div class="text-center mb-1">
                    <a href="{{ route('auth.forget.password.get') }}" class="btn btn-link">パスワードを忘れた方はこちら</a>
                </div>
		    </div>
        </div>

    @endif



@endsection

@section('script')
<script type="text/javascript">

	$("#reset_password_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		        if(response.status){
					window.location = response.url;
		        }else{
		            showValidator(response.error,'reset_password_form');
		        }
		        $("#submit_button").prop('disabled', false);
		    },
		    error:function(error){
				$("#submit_button").prop('disabled', false);
		    }
		});
	});

	$("#view_pass").on('click', async function(e){
		$("#view_pass").addClass('d-none');
		$("#hide_pass").removeClass('d-none');
		var pass = document.getElementById("password");
		if (pass.type === "password") {
			pass.type = "text";
		}
		pass.focus();
	});

	$("#hide_pass").on('click', async function(e){
		$("#hide_pass").addClass('d-none');
		$("#view_pass").removeClass('d-none');
		var pass = document.getElementById("password");
		if (pass.type === "text") {
			pass.type = "password";
		}
		pass.focus();
	});

	$("#view_pass2").on('click', async function(e){
		$("#view_pass2").addClass('d-none');
		$("#hide_pass2").removeClass('d-none');
		var pass = document.getElementById("password_confirmation");
		if (pass.type === "password") {
			pass.type = "text";
		}
		pass.focus();
	});

	$("#hide_pass2").on('click', async function(e){
		$("#hide_pass2").addClass('d-none');
		$("#view_pass2").removeClass('d-none');
		var pass = document.getElementById("password_confirmation");
		if (pass.type === "text") {
			pass.type = "password";
		}
		pass.focus();
	});

</script>
@endsection