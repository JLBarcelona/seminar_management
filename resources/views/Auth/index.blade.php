@extends('Layout.app')
@section('title', 'Login')
@section('content')
	<div class="row justify-content-center mt-3">
		<div class="col-md-5 col-12">
			<form class="needs-validation" id="login_form" action="{{ route('auth.authenticate') }}" novalidate>
				@csrf
				<!-- input start -->
				<div class="row">
					<!-- form label start -->
					<div class="col-sm-12">
						<label class="page-title">ログイン</label>
					</div>
					<!-- form label end -->
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon1">ユーザーID</span>
						  </div>
						  <input type="email" class="form-control" name="login_id" id="login_id" aria-describedby="basic-addon1">
				    	  <div class="invalid-feedback" id="err_login_id"></div>
						</div>
					</div>
					<div class="col-sm-12 col-12">
						<div class="input-group mb-3">
						  <div class="input-group-prepend bg-primary input-label">
						    <span class="input-group-text bg-primary border-0" id="basic-addon2">パスワード</span>
						  </div>
						  <input type="password" class="form-control" name="password" id="password" aria-describedby="basic-addon2" autocomplete="off">
						   <div class="input-group-append bg-primary input-label">
						    <span class="input-group-text"><i class="fa fa-eye" onclick="password_toggler(this, 'password');"></i></span>
						  </div>
						  <div class="invalid-feedback" id="err_password"></div>
						</div>
					</div>
				</div>
				<!-- input end -->
				<!-- button start -->
				<div class="row justify-content-end mb-2">
					<div class="col-sm-12 text-right mb-2">
						<div class="row justify-content-end">
							<div class="col-5"></div>
							<div class="col-7 col-md-4"><button type="submit" class="btn btn-outline-primary px-5 col-12" id="submit_button">ログイン</button></div>
						</div>						
					</div>
				</div>
				<!-- button end -->
			</form>
		</div>
	</div>
@endsection


@section('script')
<script type="text/javascript">
	$("#login_form").on('submit', function(e){
		var url = $(this).attr('action');
	    var mydata = $(this).serialize();
	    e.stopPropagation();
	    e.preventDefault(e);
	    $.ajax({
		    	type:"POST",
		        url:url,
		        data:mydata,
		        cache:false,
		        beforeSend:function(){
		           $("#submit_button").prop('disabled', true);
		        },
		    	success:function(response){
		         $("#submit_button").prop('disabled', false);
		        if(response.status == true){
		           showValidator(response.error,'login_form');
		           window.location = response.redirect;
		        }else{
		            showValidator(response.error,'login_form');
		        }
		    },
		    error:function(error){
		        console.log(error);
		        $("#submit_button").prop('disabled', false);
		    }
		});
	});
</script>

@endsection
